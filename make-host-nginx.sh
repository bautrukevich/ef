#!/bin/bash

#config
web_root='/home/web'

echo "Please enter domain or subdomain name:"
read domain &&

mkdir -p $web_root/$domain &&

echo "****************************"
echo "   Nginx settings…          "
echo "****************************"

touch /etc/nginx/sites-available/$domain &&

echo "
# $domain

server {
    listen 80;
    server_name $domain www.$domain;
    return 301 https://$domain;
}

server {
    charset utf-8;

    listen   443;

    ssl on;
    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;

    ssl_certificate    /etc/ssl/private/1_$domain\_bundle.crt;
    ssl_certificate_key    /etc/ssl/private/2_$domain.key;

    index index.php index.html index.htm;

    # Make site accessible from http://localhost/
    server_name $domain www.$domain;

    root $web_root/$domain/public;

    location / {
        try_files \$uri \$uri/ /index.php?\$query_string;
    }

    error_page 404 /404.html;
    error_page 500 502 503 504 /50x.html;
    location = /50x.html {
        root /usr/share/nginx/html;
    }

    location ~ \.php\$ {
        try_files \$uri /index.php =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)\$;
        fastcgi_pass unix:/var/run/php5-fpm.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
        include fastcgi_params;
    }

    location ~ /\. {
        deny all;
        access_log off;
        log_not_found off;
    }

}" >> /etc/nginx/sites-available/$domain &&

ln -sf /etc/nginx/sites-available/$domain /etc/nginx/sites-enabled/$domain &&

echo "****************************"
echo "   Nginx restart…           "
echo "****************************"

service nginx restart
