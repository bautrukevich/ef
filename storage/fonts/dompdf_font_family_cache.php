<?php return array (
  'sans-serif' => 
  array (
    'normal' => DOMPDF_DIR . '/lib/fonts/Helvetica',
    'bold' => DOMPDF_DIR . '/lib/fonts/Helvetica-Bold',
    'italic' => DOMPDF_DIR . '/lib/fonts/Helvetica-Oblique',
    'bold_italic' => DOMPDF_DIR . '/lib/fonts/Helvetica-BoldOblique',
  ),
  'times' => 
  array (
    'normal' => DOMPDF_DIR . '/lib/fonts/Times-Roman',
    'bold' => DOMPDF_DIR . '/lib/fonts/Times-Bold',
    'italic' => DOMPDF_DIR . '/lib/fonts/Times-Italic',
    'bold_italic' => DOMPDF_DIR . '/lib/fonts/Times-BoldItalic',
  ),
  'times-roman' => 
  array (
    'normal' => DOMPDF_DIR . '/lib/fonts/Times-Roman',
    'bold' => DOMPDF_DIR . '/lib/fonts/Times-Bold',
    'italic' => DOMPDF_DIR . '/lib/fonts/Times-Italic',
    'bold_italic' => DOMPDF_DIR . '/lib/fonts/Times-BoldItalic',
  ),
  'courier' => 
  array (
    'normal' => DOMPDF_DIR . '/lib/fonts/Courier',
    'bold' => DOMPDF_DIR . '/lib/fonts/Courier-Bold',
    'italic' => DOMPDF_DIR . '/lib/fonts/Courier-Oblique',
    'bold_italic' => DOMPDF_DIR . '/lib/fonts/Courier-BoldOblique',
  ),
  'helvetica' => 
  array (
    'normal' => DOMPDF_DIR . '/lib/fonts/Helvetica',
    'bold' => DOMPDF_DIR . '/lib/fonts/Helvetica-Bold',
    'italic' => DOMPDF_DIR . '/lib/fonts/Helvetica-Oblique',
    'bold_italic' => DOMPDF_DIR . '/lib/fonts/Helvetica-BoldOblique',
  ),
  'zapfdingbats' => 
  array (
    'normal' => DOMPDF_DIR . '/lib/fonts/ZapfDingbats',
    'bold' => DOMPDF_DIR . '/lib/fonts/ZapfDingbats',
    'italic' => DOMPDF_DIR . '/lib/fonts/ZapfDingbats',
    'bold_italic' => DOMPDF_DIR . '/lib/fonts/ZapfDingbats',
  ),
  'symbol' => 
  array (
    'normal' => DOMPDF_DIR . '/lib/fonts/Symbol',
    'bold' => DOMPDF_DIR . '/lib/fonts/Symbol',
    'italic' => DOMPDF_DIR . '/lib/fonts/Symbol',
    'bold_italic' => DOMPDF_DIR . '/lib/fonts/Symbol',
  ),
  'serif' => 
  array (
    'normal' => DOMPDF_DIR . '/lib/fonts/Times-Roman',
    'bold' => DOMPDF_DIR . '/lib/fonts/Times-Bold',
    'italic' => DOMPDF_DIR . '/lib/fonts/Times-Italic',
    'bold_italic' => DOMPDF_DIR . '/lib/fonts/Times-BoldItalic',
  ),
  'monospace' => 
  array (
    'normal' => DOMPDF_DIR . '/lib/fonts/Courier',
    'bold' => DOMPDF_DIR . '/lib/fonts/Courier-Bold',
    'italic' => DOMPDF_DIR . '/lib/fonts/Courier-Oblique',
    'bold_italic' => DOMPDF_DIR . '/lib/fonts/Courier-BoldOblique',
  ),
  'fixed' => 
  array (
    'normal' => DOMPDF_DIR . '/lib/fonts/Courier',
    'bold' => DOMPDF_DIR . '/lib/fonts/Courier-Bold',
    'italic' => DOMPDF_DIR . '/lib/fonts/Courier-Oblique',
    'bold_italic' => DOMPDF_DIR . '/lib/fonts/Courier-BoldOblique',
  ),
  'dejavu sans' => 
  array (
    'bold' => DOMPDF_DIR . '/lib/fonts/DejaVuSans-Bold',
    'bold_italic' => DOMPDF_DIR . '/lib/fonts/DejaVuSans-BoldOblique',
    'italic' => DOMPDF_DIR . '/lib/fonts/DejaVuSans-Oblique',
    'normal' => DOMPDF_DIR . '/lib/fonts/DejaVuSans',
  ),
  'dejavu sans light' => 
  array (
    'normal' => DOMPDF_DIR . '/lib/fonts/DejaVuSans-ExtraLight',
  ),
  'dejavu sans condensed' => 
  array (
    'bold' => DOMPDF_DIR . '/lib/fonts/DejaVuSansCondensed-Bold',
    'bold_italic' => DOMPDF_DIR . '/lib/fonts/DejaVuSansCondensed-BoldOblique',
    'italic' => DOMPDF_DIR . '/lib/fonts/DejaVuSansCondensed-Oblique',
    'normal' => DOMPDF_DIR . '/lib/fonts/DejaVuSansCondensed',
  ),
  'dejavu sans mono' => 
  array (
    'bold' => DOMPDF_DIR . '/lib/fonts/DejaVuSansMono-Bold',
    'bold_italic' => DOMPDF_DIR . '/lib/fonts/DejaVuSansMono-BoldOblique',
    'italic' => DOMPDF_DIR . '/lib/fonts/DejaVuSansMono-Oblique',
    'normal' => DOMPDF_DIR . '/lib/fonts/DejaVuSansMono',
  ),
  'dejavu serif' => 
  array (
    'bold' => DOMPDF_DIR . '/lib/fonts/DejaVuSerif-Bold',
    'bold_italic' => DOMPDF_DIR . '/lib/fonts/DejaVuSerif-BoldItalic',
    'italic' => DOMPDF_DIR . '/lib/fonts/DejaVuSerif-Italic',
    'normal' => DOMPDF_DIR . '/lib/fonts/DejaVuSerif',
  ),
  'dejavu serif condensed' => 
  array (
    'bold' => DOMPDF_DIR . '/lib/fonts/DejaVuSerifCondensed-Bold',
    'bold_italic' => DOMPDF_DIR . '/lib/fonts/DejaVuSerifCondensed-BoldItalic',
    'italic' => DOMPDF_DIR . '/lib/fonts/DejaVuSerifCondensed-Italic',
    'normal' => DOMPDF_DIR . '/lib/fonts/DejaVuSerifCondensed',
  ),
  'proximanovaregular' => 
  array (
    'normal' => DOMPDF_FONT_DIR . '9a4600fbcaae0233c589c965761f52fe',
  ),
  'proximanovasemibold' => 
  array (
    'normal' => DOMPDF_FONT_DIR . '7be8e2dbe9631bf0c0d021ce878087c4',
  ),
  'proximanovabold' => 
  array (
    'normal' => DOMPDF_FONT_DIR . '7be8e2dbe9631bf0c0d021ce878087c4',
  ),
  'feather' => 
  array (
    'normal' => DOMPDF_FONT_DIR . '9054d7c2163ad53fec59e82693179bb8',
  ),
  'roboto' => 
  array (
    'normal' => DOMPDF_FONT_DIR . '8c912c72d18519e1cc1eba39f41872c7',
  ),
) ?>