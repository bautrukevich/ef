# Backend Employment Application

### Требования

PHP >= 5.4 (желательно последний стабильный, например 5.6.\*)

Mcrypt PHP Extension

OpenSSL PHP Extension

Mbstring PHP Extension

Tokenizer PHP Extension (обычно встроен)

Необходимые пакеты:
```
php5-fpm php5-cli mcrypt git php5-openssl php5-json php5-curl php5-gd php5-imagick imagemagick
```

### Установка

Необходимо назначить права для чтения и записи на папку ./storage и ./bootstrap/cache/ рекурсивно. Скопировать .env.example в .env

Это можно сделать запустив:

```
sudo sh init.sh
```

Далее, необходимо отредактировать файл .env и указать необходимые настройки

### Настройки

```
# APP SETTINGS – Настройки приложения Laravel. Это можно не редактировать.
APP_ENV=production
APP_DEBUG=false
APP_KEY=jNB2ZwIeXOXJcZHIZYfvk2rDCJYoqoFI
SESSION_DRIVER=file

# Необходимо указать домен приложения
APP_DOMAIN=https://hr-process.org

# TIMEZONE — Настройки timezone, полный список тут: https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
TIMEZONE=Europe/Moscow

# API – Здесь ключ для API и его домен / ip
API_KEY=PaSiWCIUWAXhj1PLWP4HUuTpHAuzHuKOg6sM3rlwkrqoKdvQ3vepuF1DYloo
API_DOMAIN=https://hr-process.net

# Заглушка: если true, то заглушка. А если false — то сайт.
APP_CAP=true
APP_MAP_&_CONTACTS=false

COMPANY_NAME=HR PROCESS

PHONE=+7 (812) 935-11-33
EMAIL=hello@company.com

ADDRESS=г. Санкт-Петербург, Невский проспект, 52/108

MAP_ZOOM=12

MAP_MARKER_CENTER_X=59.931
MAP_MARKER_CENTER_Y=29.579

```

### NGINX

##### NGINX
Пример настройки приложения для работы с nginx (без https):
```
server {
    listen 80;
    #listen [::]:80 ipv6only=on;

    index index.php index.html index.htm;

    # Make site accessible from http://localhost/
    server_name example.com www.example.com;

    root /var/www/example.com/public # полный путь к директории public приложения

    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }

    error_page 404 /404.html;
    error_page 500 502 503 504 /50x.html;
    location = /50x.html {
        root /usr/share/nginx/html;
    }

    location ~ \.php$ {
        try_files $uri /index.php =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass unix:/var/run/php5-fpm.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
    }

}
```

##### СТРУКТУРА ФАЙЛОВ
Все логи и рабочие файлы, которые создаются приложением лежат по пути ./storage/forms/.
Если включен сайт, а не заглушка, то данные из формы собираются в файл (временно): ./storage/site/site_form.log

###### Made with Laravel 5
