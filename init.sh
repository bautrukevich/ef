#!/bin/bash

echo "****************************"
echo "  Settings permissions…     "
echo "****************************"
sudo chmod -R 777 storage/ &&

echo "****************************"
echo "  Remove temp files…        "
echo "****************************"

sudo cp .env.example .env
