<?php
namespace App\Lib\EAF;

use GuzzleHttp\Client;

use App\Lib\Markdown\MarkdownParser;
use App\Lib\Log\Log;
use App\Lib\Form\FormHelper;
use App\Lib\EAF\FormClient;

use Session;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Contracts\Filesystem\Factory;

class ApplicationFormClient extends FormClient {

	/*
	*	Get files for position information tab
	*/
	public function getFilesList($path)
	{
		$url = 'files/'.$this->type.'/'.$this->hash;

		$data = [
			'form_params' => [
				'api_token' => $this->apiKey
			]
		];

		$response = $this->client->request('POST', $url, $data);

		$response = json_decode($response->getBody()->getContents());

		$filesList = [];

		if ($response->result) {

			// Files list from API
			$files = $response->data;

			$disk = Storage::disk('local');
			$helper = new FormHelper;

			// Check if file exists
			foreach ($files as $file) {

				$fileName = $file->name;
				$fileFullName = '/'.$path.'/files/'.$file->file_name;

				// If doesn't exists — download them
				if (!$disk->exists($fileFullName)) {
					$resource = fopen(storage_path().$fileFullName, 'w+');
					$stream = \GuzzleHttp\Psr7\stream_for($resource);

					$url = 'file/'.$file->file_name;
					$response = $this->client->request('GET', $url, ['save_to' => $stream]);
				}

				$fileSize = $helper->humanFileSize(filesize(storage_path().$fileFullName));
				// echo storage_path().$fileFullName.' = '.filesize(storage_path().$fileFullName).'<br>';

				$filesList[] = [
					'fileName' => $fileName,
					'fileSize' => $fileSize,
					'fileLink' => action('FilesController@getFile', ['type' => 'forms', 'sub' => 'application', 'filename' => $file->file_name])
				];
			}

		}

		return $filesList;
	}

	/**
	* If verify phone mode is enabled get verify variables
	*/
	public function verifyPhone()
	{
		// If verify phone mode is enabled
		if ($this->data->mode_verify_phone) {

			// Detect if phone is verificated
			if (Session::has('is_verificated')) {

				$status = Session::get('is_verificated');
				if ($status == 'yes') {
					$isVerificated = true;
				} else if ($status == 'no') {
					$isVerificated = false;
				}

			} else {
				$isVerificated = false;
			}

			// If phone is verificated, get them or let empty
			$phoneNumber = Session::get('phone_number');
			$countryCode = Session::get('country_code');
			$cellPhoneNumber = Session::get('cell_phone_number');

			// Detect if phone verification is failed
			if (Session::has('is_failed')) {

				$status = Session::get('is_failed');
				if ($status == 'yes') {
					$isFailed = true;
				} else if ($status == 'no') {
					$isFailed = false;
				}

			} else {
				$isFailed = false;
			}

			// Get attempts
			$attempts = Session::get('attempts', 3);
			$attemptsNumber = $attempts;

			// Plurals
			if ($attempts == 1) {
				$attempts = $attempts." attempt";
			} else {
				$attempts = $attempts." attempts";
			}

			if (Session::has('attempts_confirmed')) {
				$attemptsConfirmed = Session::get('attempts_confirmed');
			} else {
				$attemptsConfirmed = '5';
			}

			// Plurals
			if ($attemptsConfirmed == 1) {
				$attemptsConfirmed = $attemptsConfirmed." attempt";
			} else {
				$attemptsConfirmed = $attemptsConfirmed." attempts";
			}

			$verifyPhone = [
				'isVerificated' => $isVerificated,
				'isFailed' => $isFailed,
				'attempts' => $attempts,
				'attemptsNumber' => $attemptsNumber,
				'phoneNumber' => $phoneNumber,
				'countryCode' => $countryCode,
				'cellPhoneNumber' => $cellPhoneNumber,
				'attemptsConfirmed' => $attemptsConfirmed,
			];

			return $verifyPhone;

		} else {

			$verifyPhone = [
				'isVerificated' => '',
				'isFailed' => '',
				'attempts' => '',
				'attemptsNumber' => '',
				'phoneNumber' => '',
				'countryCode' => '',
				'cellPhoneNumber' => '',
				'attemptsConfirmed' => '',
			];

			return $verifyPhone;
		}
	}

}
