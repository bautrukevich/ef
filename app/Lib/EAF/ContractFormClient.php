<?php
namespace App\Lib\EAF;

use GuzzleHttp\Client;

use App\Lib\Markdown\MarkdownParser;
use App\Lib\Log\Log;
use App\Lib\Form\FormHelper;
use App\Lib\EAF\FormClient;

use Session;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Contracts\Filesystem\Factory;

class ContractFormClient extends FormClient {

    public function getData()
    {
        // Parse Markdown text in HTML
        $this->data->tab_form_content = $this->parseInMarkdown($this->data->tab_form_content);

        if (isset($this->data->tab_info_content)) {
            $this->data->tab_info_content = $this->parseInMarkdown($this->data->tab_info_content);
        }

        $this->data->thank_you_text = $this->parseInMarkdown($this->data->thank_you_text);
        return $this->data;
    }

}
