<?php
namespace App\Lib\EAF;

use GuzzleHttp\Client;

use App\Lib\Markdown\MarkdownParser;
use App\Lib\Log\Log;
use App\Lib\Form\FormHelper;

use Session;
use Log as Logger;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Contracts\Filesystem\Factory;

class FormClient {

	public $hash;
	public $type;
	public $notify = true;

	protected $apiKey;
	protected $ip;
	protected $userAgent;

	protected $data;
	protected $iso;
	protected $accessLog;

	protected $isExists;
	protected $isEnabled;

	public function __construct()
	{
		$this->apiKey = env('API_KEY');
		$this->ip = $_SERVER['REMOTE_ADDR'];
		$this->userAgent = $_SERVER['HTTP_USER_AGENT'];

		$this->client = new Client([
		    // Base URI is used with relative requests
		    'base_uri' => env('API_DOMAIN').'/api/v1/',
		    // You can set any number of default request options.
		    'timeout'  => 0
		]);
	}

	public function get()
	{
		$url = 'forms/'.$this->type.'/'.$this->hash;

		$data = [
			'form_params' => [
				'api_token' => $this->apiKey,
				'ip' => $this->ip,
				'userAgent' => $this->userAgent,
				'notify' => $this->notify
			]
		];

		$response = $this->client->request('POST', $url, $data);

		// Logger::info('Get form response: '.var_dump($response->getBody()->getContents()));

		$response = json_decode($response->getBody()->getContents());

		if ($response->result) {

			$this->isExists = true;
			$this->isEnabled = $response->data->mode_form_is_enabled;
			$this->data = $response->data;

			$this->iso = $response->iso;
			$this->accessLog = $response->access;

		} else {
			$this->isExists = false;
		}

		return $this;
	}

	public function save($data, $type, $notifyText)
	{
		$url = 'forms/'.$type;

		$data['form_params']['api_token'] = $this->apiKey;
		$data['form_params']['notify'] = $notifyText;

		$response = $this->client->request('POST', $url, $data);

		$response = json_decode($response->getBody()->getContents());
		return $response;
	}

	public function isExists()
	{
		return $this->isExists;
	}

	public function isEnabled()
	{
		return $this->isEnabled;
	}

	public function getData()
	{
		// Parse Markdown text in HTML
		$this->data->tab_form_content = $this->parseInMarkdown($this->data->tab_form_content);

		if (isset($this->data->tab_info_content)) {
			$this->data->tab_info_content = $this->parseInMarkdown($this->data->tab_info_content);
		}

		$this->data->tab_form_agreement = $this->parseInMarkdown($this->data->tab_form_agreement);
		$this->data->thank_you_text = $this->parseInMarkdown($this->data->thank_you_text);
		return $this->data;
	}

	public function getISO()
	{
		return $this->iso;
	}

	public function getIP()
	{
		return $this->ip;
	}

	/*
	*	Parse Markdown in HTML
	*/
	public function parseInMarkdown($text)
	{
		// Parse Markdown
		$parser = new MarkdownParser('github');
		$text = $parser->parse($text);

		return $text;
	}

	/*
	*	Write access.log and unique.log
	*/
	public function writeAccessLog($type)
	{
		if ($this->data->mode_write_log_access) {
			$log = new Log($type.'/logs');
			$log->iso = $this->iso;
			$accessLog = $log->writeAccessLog($this->accessLog);
			return $accessLog;
		}
	}

	/*
	*	Write ip.log
	*/
	public function writeIpLog($type)
	{
		// Write ip.log
		if ($this->data->mode_write_log_ip) {
			$log = new Log($type.'/logs');
			$log->iso = $this->iso;
			$ipLog = $log->writeIpLog();
			return $ipLog;
		}
	}

	public function sendFileName($file, $type, $entryId)
	{
		$url = 'user-file';

		$data = [
			'form_params' => [
				'api_token' => $this->apiKey,
				'hash' => $this->hash,
				'form_type' => $this->type,
				'entry_id' => $entryId,
		        'file' => $file,
				'type' => $type
		    ]
		];

		$response = $this->client->request('POST', $url, $data);
		$response = json_decode($response->getBody()->getContents());

		return $response;
	}

	public static function sendSMS($phoneNumber, $verificationText)
	{
		$client = new Client([
			// Base URI is used with relative requests
			'base_uri' => env('API_DOMAIN').'/api/v1/',
			// You can set any number of default request options.
			'timeout'  => 0,
		]);

		$url = 'sms';

		$data = [
			'form_params' => [
				'api_token' => env('API_KEY'),
				'phoneNumber' => $phoneNumber,
				'verificationText' => $verificationText,
			]
		];

		$response = $client->request('POST', $url, $data);
		$response = json_decode($response->getBody()->getContents());

		return $response;
	}

	public function getEmployeeData($email)
	{
		$client = new Client([
			// Base URI is used with relative requests
			'base_uri' => env('API_DOMAIN').'/api/v1/',
			// You can set any number of default request options.
			'timeout'  => 0,
		]);

		$url = 'employee';

		$data = [
			'form_params' => [
				'api_token' => env('API_KEY'),
				'email' => $email
			]
		];

		$response = $client->request('POST', $url, $data);
		$response = json_decode($response->getBody()->getContents());

		return $response;
	}

}
