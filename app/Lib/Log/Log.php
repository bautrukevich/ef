<?php

namespace App\Lib\Log;

use App\Lib\TabGeo\TabGeo;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Contracts\Filesystem\Factory;

class Log {

	public $path;
	public $ip;
	public $userAgent;
	public $iso;

	public function __construct($path)
	{
		$this->path = $path;
		$this->ip = $_SERVER['REMOTE_ADDR'];
		$this->userAgent = $_SERVER['HTTP_USER_AGENT'];
	}

	public function writeAllLogs()
	{
		$ipLog = $this->writeIpLog();
		$accessLog = $this->writeAccessLog();

		return ['ipLog' => $ipLog, 'accessLog' => $accessLog];
	}

	public function writeIpLog()
	{

		$logString = $this->ip . "\n";

		$disk = Storage::disk('local');

		if ($disk->exists($this->path.'/ip.log')) {
			$disk->append($this->path.'/ip.log', $logString);
		} else {
			$disk->put($this->path.'/ip.log', $logString);
		}

		return $logString;
	}

	public function writeAccessLog($logString = '')
	{
		if ($logString == '') {
			$logString = date('d F Y, H:i') . ' UTC. => IP: ' . $this->ip .
			'   Country: ' . $this->iso  . '   UserAgent: ' . $this->userAgent . "\n";
		}

		$disk = Storage::disk('local');

		if ($disk->exists($this->path.'/access.log')) {
			$disk->append($this->path.'/access.log', $logString);
		} else {
			$disk->put($this->path.'/access.log', $logString);
		}

		if (!$this->isIpRepeat()) {

			if ($disk->exists($this->path.'/unique.log')) {
				$disk->append($this->path.'/unique.log', $logString);
			} else {
				$disk->put($this->path.'/unique.log', $logString);
			}

		}

		return $logString;
	}


	public function getAccessLog()
	{
		$logString = date('d F Y, H:i') . ' UTC. => IP: ' . $this->ip .
		'   Country: ' . $this->iso  . '   UserAgent: ' . $this->userAgent . "\n";

		return $logString;
	}

	protected function isIpRepeat()
	{

		$disk = Storage::disk('local');

		if (!$disk->exists($this->path.'/ip.log')) { return false; }

		$file = $disk->get($this->path.'/ip.log');

		if (!strstr($file, "\n")) { return false; }

		$ipsArray = explode("\n", $file);

		if(in_array($this->ip, $ipsArray)){
			return true;
		} else {
			return false;
		}

	}

}
