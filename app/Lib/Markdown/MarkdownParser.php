<?php

namespace App\Lib\Markdown;

use \cebe\markdown\Markdown as Markdown;
use \cebe\markdown\GithubMarkdown as GithubMarkdown;
use \cebe\markdown\MarkdownExtra as MarkdownExtra;

/**
 * Markdown Parser
 */
class MarkdownParser
{
    public $parser;

    function __construct($parser)
    {
        $this->parser = $parser;
    }

    public function parse($markdown)
    {
        switch ($this->parser) {
            case 'markdown':
                // traditional markdown and parse full text
                $parser = new Markdown();
                $html = $parser->parse($markdown);
                break;
            case 'github':
                // use github markdown
                $parser = new GithubMarkdown();
                $html = $parser->parse($markdown);
                break;
            case 'extra':
                // use markdown extra
                $parser = new MarkdownExtra();
                $html = $parser->parse($markdown);
                break;

            default:
                // traditional markdown and parse full text
                $parser = new Markdown();
                $html = $parser->parse($markdown);
                break;
        }

        return $html;
    }

    private function example()
    {
        // traditional markdown and parse full text
        $parser = new Markdown();
        $html = $parser->parse($markdown);

        // use github markdown
        $parser = new GithubMarkdown();
        $html = $parser->parse($markdown);

        // use markdown extra
        $parser = new MarkdownExtra();
        $html = $parser->parse($markdown);

        // parse only inline elements (useful for one-line descriptions)
        $parser = new GithubMarkdown();
        $html = $parser->parseParagraph($markdown);
    }

}
