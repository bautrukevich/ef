<?php

namespace App\Lib\Form;

class FormHelper {

	// Sanitaze data: strip_tags, etc.
	public function sanitaze($badData)
	{
		$goodData = [];
		foreach ($badData as $key => $value) {
			$goodData[$key] = $this->clear($value);
		}

		return $goodData;
	}

	// Strip tags
	public function clear($string){
		if(!$string){
			return null;
		}
		$tmp = trim(strip_tags($string));
		if(!strlen($tmp)){return '';}

		return $this->rn2br(strtr($tmp, array('"'=>"'",'{'=>'','}'=>'',';'=>'',)));
	}

	// Replace new line to <br>
	public function rn2br($input){
		$input = preg_replace("/\r\n/i", '<br>', $input);
		return preg_replace("/\n/i", '<br>', $input);
	}

	// Return human filesize
	public function humanFileSize($size, $precision = 2) {
	    $units = array('B','KB','MB','GB','TB','PB','EB','ZB','YB');
	    $step = 1024;
	    $i = 0;
	    while (($size / $step) > 0.9) {
	        $size = $size / $step;
	        $i++;
	    }
	    return round($size, $precision)." ".$units[$i];
	}

}
