<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Welcome
if (env('APP_CAP')) {
	Route::get('/', 'WelcomeController@index');
} else {
	Route::get('/', 'PagesController@index');
  Route::get('/how-it-works', 'PagesController@howItWorks');
  Route::get('/contacts', 'PagesController@contacts');
	Route::post('/send', 'PagesController@send');
}


// Tests
if (env('APP_DEBUG')) {
	Route::get('/session-destroy', 'TestsController@sessionDestroy');
	Route::get('/session-all', 'TestsController@sessionAll');
	Route::get('/get-data', 'TestsController@getData');
	Route::get('/send-data', 'TestsController@sendData');
	Route::get('/check-jabber', 'TestsController@checkJabber');
	Route::get('/get-files', 'TestsController@getFiles');
}

// Phone verification
Route::post('/get-code', 'AjaxController@getVerificationCode');
Route::post('/get-attempts', 'AjaxController@getAttempts');
Route::post('/confirm-code', 'AjaxController@confirmVerificationCode');
Route::post('/set-failed', 'AjaxController@setVerificationFailed');
Route::post('/get-status', 'AjaxController@getVerificationStatus');
Route::post('/get-employee-data', 'AjaxController@getEmployeeData');

// Forms
Route::group(['prefix' => '/f/'], function() {
	Route::get('application/{id}', 'ApplicantsController@create');
	Route::get('account/{id}', 'AccountsController@create');
	Route::get('contract/{id}', 'ContractsController@create');
});

// Documents
// Route::group(['prefix' => '/d/'], function() {
// 	Route::get('{id}', 'DocumentsController@create');
//
// 	Route::post('{id}', 'DocumentsController@store');
// });

// Save application form
Route::post('/applications/{form}', 'ApplicantsController@store');

// Save account form
Route::post('/accounts/{form}', 'AccountsController@store');

// Save contract form
Route::post('/contracts/{form}', 'ContractsController@store');

// Get country prefix
Route::post('/update-prefixes', 'AjaxController@getPhonePrefixByIso');

// Get files
// Get PDF file from storage
Route::get('/pdf/{type}/{sub}/{filename}', 'FilesController@getPdf');
// Get uploaded file from storage
Route::get('/user-file/{type}/{sub}/{filename}', 'FilesController@getUserFile');
// Get uploaded file from storage
Route::get('/file/{type}/{sub}/{filename}', 'FilesController@getFile');

// Get uploaded signature file from storage
Route::get('/signature/{type}/{sub}/{filename}', 'FilesController@getSignature');
