<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class ApplicantRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'firstName' => 'sometimes|required|alpha',
			'lastName' => 'sometimes|required|alpha',
			'gender' => 'sometimes|required|alpha',
			'bMonth' => 'sometimes|required|alpha',
			'bDay' => 'sometimes|required|numeric',
			'bYear' => 'sometimes|required|numeric',
			'addressLine1' => 'sometimes|required|alpha_num',
			'addressLine2' => 'sometimes|required|alpha_num',
			'city' => 'sometimes|required|alpha',
			'region' => 'sometimes|required|alpha_num',
			'zip' => 'sometimes|required|numeric',
			'country' => 'sometimes|required|alpha',
			'homePhoneCountryCode' => 'sometimes|required',
			'homePhoneNumber' => 'sometimes|required|numeric',
			'cellPhoneCountryCode' => 'sometimes|required',
			'cellPhoneNumber' => 'sometimes|required|numeric',
			'bestTime' => 'sometimes|required|alpha_num',
			'email' => 'sometimes|required|email',
			'employmentStatus' => 'sometimes|required|alpha',
			'employmentStart' => 'sometimes|required|alpha_num',
			'resume' => 'sometimes|mimes:jpg,jpeg,bmp,png,pdf,doc,docx'
		];
	}

}
