<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\ApplicantRequest;
use App\Http\Controllers\Controller;

use PDF;
use Carbon\Carbon;
use Session;
use Mail;
use Validator;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Contracts\Filesystem\Factory;

use App\Lib\Form\FormHelper;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use GuzzleHttp\Client;

use App\Lib\Markdown\MarkdownParser;

use Imagick;

use App\Lib\EAF\ApplicationFormClient;

class ApplicantsController extends Controller {

	/**
	* Show the form for creating a new resource.
	*
	* @return Response
	*/
	public function create($id)
	{
		// Create API Client
		$client = new ApplicationFormClient();
		$client->type = 'application';
		$client->hash = $id;
		$client->notify = true;
		$client->get();

		// If Form is exists and enabled
		if ($client->isExists() && $client->isEnabled()) {

			// Write access.log and unique.log
			$accessLog = $client->writeAccessLog('forms/'.$client->type);

			// Write ip.log
			$ipLog = $client->writeIpLog('forms/'.$client->type);

			// Get form data for create view
			$form = $client->getData();

			// Get IP and ISO
			$ip = $client->getIP();
			$iso = $client->getISO();

			// If verify phone mode is enabled
			$verifyPhone = $client->verifyPhone();

			// Get files list
			$filesList = $client->getFilesList('forms/'.$client->type);

			// Get now time and date
			$now = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now());

			return view('applications.create', compact('form', 'id', 'iso', 'ip', 'now', 'verifyPhone', 'filesList'));

		} else {
			abort(404);
		}
	}

	/**
	*	Store a newly created resource in storage.
	*
	*	@return Response
	*/
	public function store(Request $request, $id)
	{
		// Create API Client
		$client = new ApplicationFormClient();
		$client->type = 'application';
		$client->hash = $id;
		$client->notify = false;
		$client->get();

		// Get form data for create view
		$form = $client->getData();

		// Receive and prepare data
		$data = $this->prepareData($request, $form);
		$signatureOutput = $request->signatureOutput;

		// Prepare to send via API
		$saveData = $this->getSaveData($data, $form);

		$shortData = $this->getShortData($saveData, $form);
		$fullData = $this->getFullData($saveData, $form, true);

		$download = [];

		// Photo upload block
		if ($form->block_photo) {
			$filePhoto = $this->uploadPhoto($request, $data);

			if ($filePhoto) {
				$download['photo'] = action('FilesController@getUserFile', ['type' => 'forms', 'sub' => 'application', 'filename' => $filePhoto]);
				// $client->sendFileName($filePhoto, 'photo', $entryId);
			}
		}

		// Resume upload block
		if ($form->block_resume) {
			$fileResume = $this->uploadResume($request, $data);

			if ($fileResume) {
				$download['resume'] = action('FilesController@getUserFile', ['type' => 'forms', 'sub' => 'application', 'filename' => $fileResume]);
				// $client->sendFileName($fileResume, 'resume', $entryId);
			}
		}

		// Signature block
		if ($form->block_signature) {
			$signature = $this->saveSignature($data, $signatureOutput);
			$download['signature'] = $signature['download'];
			$data['signatureFile'] = $signature['signatureFile'];
			$data['signatureFileName'] = $signature['signatureFileName'];

			// $client->sendFileName($signature['fileName'], 'signature', $entryId);
		}

		// Generate PDF
		if ($form->mode_form_output == 'pdf') {
			$pdfResult = $this->savePDF($data, $form);
			$download['pdf'] = $pdfResult['downloadPdf'];
			$pdfFileName = $pdfResult['pdfFileName'];

			// $client->sendFileName($pdfFileName, 'pdf', $entryId);
		}

		$notifySubmitText = $this->notifySubmit($download, $shortData);

		// Send data and notify submit text via API
		$entryId = $client->save(['form_params' => $saveData], 'application', $notifySubmitText);

		if (isset($download['photo'])) {
			$client->sendFileName($filePhoto, 'photo', $entryId);
		}

		if (isset($download['resume'])) {
			$client->sendFileName($fileResume, 'resume', $entryId);
		}

		if (isset($download['signature'])) {
			$client->sendFileName($signature['fileName'], 'signature', $entryId);
		}

		if (isset($download['pdf'])) {
			$client->sendFileName($pdfResult['fileName'], 'pdf', $entryId);
		}

		Session::flush();

		// If forn mode = pdf
		if ($form->mode_form_output == 'pdf') {
			// Return response
			return response()->json(['status' => 'success', 'link' => $pdfFileName]);
		} else {
			// Return response
			return response()->json(['status' => 'success']);
		}
	}

	/*
	*	Prepare data
	*/
	private function prepareData(Request $request, $form)
	{

		// // Receive data except
		$data = $request->except('_token', 'photo', 'resume', 'submit');

		$fields = ['firstName', 'middleName', 'lastName', 'bMonth', 'bDay', 'bYear', 'cellPhoneNumber', 'isPhoneConfirmed', 'email', 'linkedin', 'facebook', 'institution', 'degree', 'major', 'educationCountry', 'educationDescription', 'addressLine1', 'addressLine2', 'city', 'region', 'zip', 'addressCountry', 'employmentStatus', 'employmentStart','preferredJobType','bestTimeToCall', 'occupation', 'company', 'experienceDescription', 'salary', 'signature'];

		foreach ($fields as $field) {
			if (!isset($data[$field])) {
				$data[$field] = null;
			}
		}

		// Form sanitaze
		$formHelper = new FormHelper;
		$data = $formHelper->sanitaze($data);

		$upper = ['firstName', 'middleName', 'lastName', 'institution', 'degree', 'major', 'educationDescription', 'addressLine1', 'addressLine2', 'city', 'region', 'occupation', 'company', 'experienceDescription', 'salary'];

		// String to uppercase
		foreach ($upper as $field) {
			$data[$field] = ucfirst($data[$field]);
		}

		// Format birthday date
		$data['bDate'] = $data['bMonth'].' '.$data['bDay'].', '.$data['bYear'];

		// Compose cell and home phone number
		$data['cellPhone'] = $data['cellPhoneNumber'];

		// In VERIFY_PHONE mode
		if ($form->mode_verify_phone) {

			if (Session::has('is_verificated')) {

				$status = Session::get('is_verificated');
				if ($status == 'yes') {
					$status = 'Phone was confirmed';
				} else {
					$status = 'Phone was not confirmed';
				}
				$data["isPhoneConfirmed"] = $status;

			}

		} else {
			$data["isPhoneConfirmed"] = null;
		}

		// Compose education
		if (isset($data['studyingMonthStart']) && isset($data['studyingYearStart'])) {
			$data['educationStart'] = $data['studyingMonthStart'].', '.$data['studyingYearStart'];
		} else {
			$data['educationStart'] = null;
		}

		if (isset($data['currentAttend']) && $data['currentAttend'] !== '') {
			$data['educationEnd'] = $data['currentAttend'];
		} else {
			if (isset($data['studyingMonthEnd']) && isset($data['studyingYearEnd'])) {
				$data['educationEnd'] = $data['studyingMonthEnd'].', '.$data['studyingYearEnd'];
			} else {
				$data['educationStart'] = null;
			}
		}

		// Compose experience
		if (isset($data['employmentMonthStart']) && isset($data['employmentYearStart'])) {
			$data['employmentHistoryStart'] = $data['employmentMonthStart'].', '.$data['employmentYearStart'];
		} else {
			$data['employmentHistoryStart'] = null;
		}

		if (isset($data['currentWork']) && $data['currentWork'] !== '') {
			$data['employmentHistoryEnd'] = $data['currentWork'];
		} else {
			if (isset($data['employmentMonthEnd']) && isset($data['employmentYearEnd'])) {
				$data['employmentHistoryEnd'] = $data['employmentMonthEnd'].', '.$data['employmentYearEnd'];
			} else {
				$data['employmentHistoryEnd'] = null;
			}
		}

		// If date was not passed (was removed)
		if (!isset($data['date']) || $data['date'] == '') {
			$now = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now());
			$data['date'] = Carbon::parse($now)->format('Y-m-d H:i:s');
		}

		// If IP was not passed (was removed)
		if (!isset($data['ip']) || $data['ip'] == '') {
			$data['ip'] = $_SERVER['REMOTE_ADDR'];
		}

		// User Agent
		if (!isset($data['userAgent']) || $data['userAgent'] == '') {
			$data['userAgent'] = $_SERVER['HTTP_USER_AGENT'];
		}

		return $data;
	}

	/*
	*	Get full data
	*/
	private function getShortData($data, $form)
	{
		$fields = ['employmentStatus', 'employmentStart', 'isPhoneConfirmed'];

		foreach ($fields as $field) {
			if (!isset($data[$field])) {
				$data[$field] = null;
			}
		}

		$shortArray = explode(";", $form->short_data);
		$shortData = [];

		foreach ($shortArray as $key => $value) {
			$shortData[] = $data[$value];
		}

		return $shortData;
	}

	/*
	*	Get full data
	*/
	private function getFullData($data, $form, $assoc = false)
	{
		$fields = ['educationStart', 'educationEnd', 'employmentHistoryStart', 'employmentHistoryEnd', 'isPhoneConfirmed', 'signature'];

		foreach ($fields as $field) {
			if (!isset($data[$field])) {
				$data[$field] = null;
			}
		}

		$fullArray = explode(";", $form->full_data);
		$fullData = [];

		foreach ($fullArray as $key => $value) {
			$fullData[] = $data[$value];
		}

		return $fullData;
	}

	/*
	*	Upload photo
	*/
	private function uploadPhoto(Request $request, $data)
	{
		// If file was selected
		if ($request->hasFile('photo')) {

			// Validate MIME-type. If validation fails return json with 422 statusCode and errors
			$validationErrors = $this->validate($request, ['photo' => 'sometimes|mimes:jpg,jpeg,bmp,png']);

			if ($validationErrors == null) {

				// Upload file
				$file = $request->file('photo');

				// If file was uploaded successfully
				if ($file->isValid()) {

					// Get original filename and extension
					$uploadedFile = explode(".", $file->getClientOriginalName());

					// Get original filename and extension
                    $uploadedFileName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME); // file
                    $uploadedFileExt = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION); // jpg

					// Set new name with same extension
					$now = str_replace([":"], "-", str_replace([" "], "_", Carbon::now()));
					$newFileName = preg_replace('|[^a-zA-Z_]|', '', ($data['lastName'] . '_' . $data['firstName'][0] . '_')) . substr(md5(time() . rand(1,999)), 1, 4).'_'.$now.'_photo.'.$uploadedFileExt;

					// Move file to our folder
					$file->move(storage_path()."/forms/application/user_files/", $newFileName);

					// Set downloadFileString
					return $newFileName;

				} else {

					// File was not uploaded
					return response()->json(['status' => 'error', 'errors' => 'Photo was not uploaded']);

				}

			} else {

				return response()->json(['status' => 'error', 'errors' => $validationErrors]);

			}

		} else {

			return false;

		}

	}

	/*
	*	Upload resume
	*/
	private function uploadResume(Request $request, $data)
	{
		// If file was selected
		if ($request->hasFile('resume')) {

			// Validate MIME-type. If validation fails return json with 422 statusCode and errors
			$validationErrors = $this->validate($request, ['resume' => 'sometimes|mimes:jpg,jpeg,bmp,png,pdf,doc,docx,txt']);

			if ($validationErrors == null) {

				// Upload file
				$file = $request->file('resume');

				// If file was uploaded successfully
				if ($file->isValid()) {

					// Get original filename and extension
					$uploadedFile = explode(".", $file->getClientOriginalName());

					// Get original filename and extension
                    $uploadedFileName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME); // file
                    $uploadedFileExt = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION); // jpg

					// Set new name with same extension
					$now = str_replace([":"], "-", str_replace([" "], "_", Carbon::now()));
					$newFileName = preg_replace('|[^a-zA-Z_]|', '', ($data['lastName'] . '_' . $data['firstName'][0] . '_')) . substr(md5(time() . rand(1,999)), 1, 4).'_'.$now.'_resume.'.$uploadedFileExt;

					// Move file to our folder
					$file->move(storage_path()."/forms/application/user_files/", $newFileName);

					// Set downloadFileString
					return $newFileName;

				} else {

					// File was not uploaded
					return response()->json(['status' => 'error', 'errors' => 'Resume was not uploaded']);

				}

			} else {

				return response()->json(['status' => 'error', 'errors' => $validationErrors]);

			}

		} else {

			return false;

		}

	}

	/*
	*	Save signature
	*/
	private function saveSignature($data, $signatureOutput)
	{
		// Set signature file name
		$now = str_replace([":"], "-", str_replace([" "], "_", Carbon::now()));
		$signatureFileName = preg_replace('|[^a-zA-Z_]|', '', ($data['lastName'] . '_' . $data['firstName'][0] . '_')) . substr(md5(time() . rand(1,999)), 1, 4).'_'.$now.'_signature.png';

		// Convert base64 to png and save them
		$img = $signatureOutput;
		$img = str_replace('data:image/png;base64,', '', $img);
		$img = str_replace(' ', '+', $img);
		$img = base64_decode($img);
		Storage::put('forms/application/signatures/'.$signatureFileName, $img);

		$imgFile = storage_path().'/forms/application/signatures/'.$signatureFileName;

		// Remove blanks
		$img = new Imagick($imgFile);
		$img->trimImage(0);
		$img->writeImage($imgFile);

		return $signature = [
			'signatureFile' => action('FilesController@getSignature', ['type' => 'forms', 'sub' => 'application', 'filename' => $signatureFileName]),
			'signatureFileName' => $imgFile,
			'fileName' => $signatureFileName,
			'download' => action('FilesController@getSignature', ['type' => 'forms', 'sub' => 'application', 'filename' => $signatureFileName])
		];
	}

	// Prepare data to send to API for saving
	private function getSaveData($data, $form)
	{
		$saveData['hash'] = $form->hash;
		$saveData['email'] = isset($data['email']) ? $data['email'] : null;
		$saveData['first_name'] = isset($data['firstName']) ? $data['firstName'] : null;
		$saveData['middle_name'] = isset($data['middleName']) ? $data['middleName'] : null;
		$saveData['last_name'] = isset($data['lastName']) ? $data['lastName'] : null;

		$saveData['b_date'] = isset($data['bDate']) ? $data['bDate'] : null;

		$saveData['cell_phone'] = isset($data['cellPhone']) ? $data['cellPhone'] : null;
		$saveData['phone_is_confirmed'] = isset($data['isPhoneConfirmed']) ? $data['isPhoneConfirmed'] : null;

		$saveData['linkedin'] = isset($data['linkedin']) ? $data['linkedin'] : null;
		$saveData['facebook'] = isset($data['facebook']) ? $data['facebook'] : null;

		$saveData['institution'] = isset($data['institution']) ? $data['institution'] : null;
		$saveData['degree'] = isset($data['degree']) ? $data['degree'] : null;
		$saveData['major'] = isset($data['major']) ? $data['major'] : null;
		$saveData['education_country'] = isset($data['educationCountry']) ? $data['educationCountry'] : null;
		$saveData['education_start'] = isset($data['educationStart']) ? $data['educationStart'] : null;
		$saveData['education_end'] = isset($data['educationEnd']) ? $data['educationEnd'] : null;
		$saveData['education_description'] = isset($data['educationDescription']) ? $data['educationDescription'] : null;

		$saveData['address_line_1'] = isset($data['addressLine1']) ? $data['addressLine1'] : null;
		$saveData['address_line_2'] = isset($data['addressLine2']) ? $data['addressLine2'] : null;
		$saveData['city'] = isset($data['city']) ? $data['city'] : null;
		$saveData['region'] = isset($data['region']) ? $data['region'] : null;
		$saveData['zip'] = isset($data['zip']) ? $data['zip'] : null;
		$saveData['address_country'] = isset($data['addressCountry']) ? $data['addressCountry'] : null;

		$saveData['employment_status'] = isset($data['employmentStatus']) ? $data['employmentStatus'] : null;
		$saveData['employment_start'] = isset($data['employmentStart']) ? $data['employmentStart'] : null;
		$saveData['preferred_job_type'] = isset($data['preferredJobType']) ? $data['preferredJobType'] : null;
		$saveData['best_time_to_call'] = isset($data['bestTimeToCall']) ? $data['bestTimeToCall'] : null;

		$saveData['occupation'] = isset($data['occupation']) ? $data['occupation'] : null;
		$saveData['company'] = isset($data['company']) ? $data['company'] : null;
		$saveData['experience_description'] = isset($data['experienceDescription']) ? $data['experienceDescription'] : null;
		$saveData['salary'] = isset($data['salary']) ? $data['salary'] : null;

		$saveData['employment_history_start'] = isset($data['employmentHistoryStart']) ? $data['employmentHistoryStart'] : null;
		$saveData['employment_history_end'] = isset($data['employmentHistoryEnd']) ? $data['employmentHistoryEnd'] : null;

		$saveData['signature'] = isset($data['signature']) ? $data['signature'] : null;
		$saveData['date'] = isset($data['date']) ? $data['date'] : null;
		$saveData['ip'] = isset($data['ip']) ? $data['ip'] : null;
		$saveData['user_agent'] = isset($data['userAgent']) ? $data['userAgent'] : null;

		return $saveData;
	}

	/*
	*	Send PDF
	*/
	private function savePDF($data, $form)
	{
		$data['form'] = $form;

		// Set filename
		$now = str_replace([":"], "-", str_replace([" "], "_", Carbon::now()));
		$pdfFileName = preg_replace('|[^a-zA-Z_]|', '', ($data['firstName'] . '_' . $data['lastName'] . '_')) . substr(md5(time() . rand(1,999)), 1, 4) . '_' . $now.'.pdf';

		// Create and save pdf
		$pdfFilePath = storage_path()."/forms/application/pdf/".$pdfFileName;
		$pdf = PDF::loadView('applications.pdf', $data)->save($pdfFilePath);
		$downloadPdfString = url("/pdf/forms/application/".$pdfFileName);
		return $result = [
			'downloadPdf' => $downloadPdfString,
			'pdfFileName' => 'pdf/forms/application/'.$pdfFileName,
			'fileName' => $pdfFileName
		];
	}

	private function notifySubmit($download, $shortData)
	{
		$resultJabberString = "New form was submitted. Details: \n";

		if (isset($shortData)) {
			$shortText = implode(";", $shortData);
			$resultJabberString = $resultJabberString.$shortText."\n";
		}

		if (isset($download['pdf'])) {
			$resultJabberString = $resultJabberString."Download the form in PDF: ".$download['pdf']."\n";
		}

		if (isset($download['photo'])) {
			$resultJabberString = $resultJabberString."Uploaded photo: ".$download['photo']."\n";
		}

		if (isset($download['resume'])) {
			$resultJabberString = $resultJabberString."Uploaded resume: ".$download['resume']."\n";
		}

		if (isset($download['signature'])) {
			$resultJabberString = $resultJabberString."Signature: ".$download['signature']."\n";
		}

		return $resultJabberString;

	}

}
