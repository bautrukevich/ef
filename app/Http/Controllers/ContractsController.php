<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\ApplicantRequest;
use App\Http\Controllers\Controller;

use PDF;
use Carbon\Carbon;
use Session;
use Mail;
use Validator;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Contracts\Filesystem\Factory;

use App\Lib\Form\FormHelper;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use GuzzleHttp\Client;

use App\Lib\Markdown\MarkdownParser;

use Imagick;

use App\Form;
use App\Entry;

use App\Lib\EAF\ContractFormClient;

class ContractsController extends Controller {

	/**
	* Show the form for creating a new resource.
	*
	* @return Response
	*/
	public function create($id)
	{
		// Create API Client
		$client = new ContractFormClient();
		$client->type = 'contract';
		$client->hash = $id;
		$client->notify = true;
		$client->get();

		// If Form is exists and enabled
		if ($client->isExists() && $client->isEnabled()) {

			// Write access.log and unique.log
			$accessLog = $client->writeAccessLog('forms/'.$client->type);

			// Write ip.log
			$ipLog = $client->writeIpLog('forms/'.$client->type);

			// Get form data for create view
			$form = $client->getData();

			// Get IP and ISO
			$ip = $client->getIP();
			$iso = $client->getISO();

			// Get now time and date
			$now = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now());

			return view('contracts.create', compact('form', 'id', 'iso', 'ip', 'now'));

		} else {
			abort(404);
		}
	}

	/**
	*	Store a newly created resource in storage.
	*
	*	@return Response
	*/
	public function store(Request $request, $id)
	{
		// Create API Client
		$client = new ContractFormClient();
		$client->type = 'contract';
		$client->hash = $id;
		$client->notify = false;
		$client->get();

		// Get form data for create view
		$form = $client->getData();

		// Receive and prepare data
		$data = $this->prepareData($request, $form);
		$signatureOutput = $request->signatureOutput;

		// Prepare to send via API
		$saveData = $this->getSaveData($data, $form);

		$shortData = $this->getShortData($saveData, $form);
		$fullData = $this->getFullData($saveData, $form, true);

		// Signature block
		$signature = $this->saveSignature($data, $signatureOutput);
		$download['signature'] = $signature['download'];
		$data['signatureFile'] = $signature['signatureFile'];
		$data['signatureFileName'] = $signature['signatureFileName'];

		$download = [];

		// Generate PDF
		if ($form->mode_form_output == 'pdf') {
			$pdfResult = $this->savePDF($data, $form);
			$download['pdf'] = $pdfResult['downloadPdf'];
			$pdfFileName = $pdfResult['pdfFileName'];
		}

		$notifySubmitText = $this->notifySubmit($download, $shortData);

		// Send data and notify submit text via API
		$entryId = $client->save(['form_params' => $saveData], 'contract', $notifySubmitText);

		if (isset($download['signature'])) {
			$client->sendFileName($signature['fileName'], 'signature', $entryId);
		}

		if (isset($download['pdf'])) {
			$client->sendFileName($pdfResult['fileName'], 'pdf', $entryId);
		}

		Session::flush();

		// If forn mode = pdf
		if ($form->mode_form_output == 'pdf') {
			// Return response
			return response()->json(['status' => 'success', 'link' => $pdfFileName]);
		} else {
			// Return response
			return response()->json(['status' => 'success']);
		}
	}

	/*
	*	Prepare data
	*/
	private function prepareData(Request $request, $form)
	{

		// // Receive data except
		$data = $request->except('_token', 'submit');

		$fields = ['name', 'email', 'signature'];

		foreach ($fields as $field) {
			if (!isset($data[$field])) {
				$data[$field] = null;
			}
		}

		// Form sanitaze
		$formHelper = new FormHelper;
		$data = $formHelper->sanitaze($data);

		// If date was not passed (was removed)
		if (!isset($data['date']) || $data['date'] == '') {
			$now = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now());
			$data['date'] = Carbon::parse($now)->format('Y-m-d H:i:s');
		}

		// If IP was not passed (was removed)
		if (!isset($data['ip']) || $data['ip'] == '') {
			$data['ip'] = $_SERVER['REMOTE_ADDR'];
		}

		// User Agent
		if (!isset($data['userAgent']) || $data['userAgent'] == '') {
			$data['userAgent'] = $_SERVER['HTTP_USER_AGENT'];
		}

		return $data;
	}

	/*
	*	Get full data
	*/
	private function getShortData($data, $form)
	{
		$fields = ['name', 'email'];

		foreach ($fields as $field) {
			if (!isset($data[$field])) {
				$data[$field] = null;
			}
		}

		$shortArray = explode(";", $form->short_data);
		$shortData = [];

		foreach ($shortArray as $key => $value) {
			$shortData[] = $data[$value];
		}

		return $shortData;
	}

	/*
	*	Get full data
	*/
	private function getFullData($data, $form, $assoc = false)
	{
		$fields = ['name', 'email', 'signature'];

		foreach ($fields as $field) {
			if (!isset($data[$field])) {
				$data[$field] = null;
			}
		}

		$fullArray = explode(";", $form->full_data);
		$fullData = [];

		foreach ($fullArray as $key => $value) {
			$fullData[] = $data[$value];
		}

		return $fullData;

	}

	/*
	*	Save signature
	*/
	private function saveSignature($data, $signatureOutput)
	{
		// Set signature file name
		$now = str_replace([":"], "-", str_replace([" "], "_", Carbon::now()));
		$signatureFileName = preg_replace('|[^a-zA-Z_]|', '', $data['name']) .'_'. substr(md5(time() . rand(1,999)), 1, 4).'_'.$now.'_signature.png';

		// Convert base64 to png and save them
		$img = $signatureOutput;
		$img = str_replace('data:image/png;base64,', '', $img);
		$img = str_replace(' ', '+', $img);
		$img = base64_decode($img);
		Storage::put('forms/contract/signatures/'.$signatureFileName, $img);

		$imgFile = storage_path().'/forms/contract/signatures/'.$signatureFileName;

		// Remove blanks
		$img = new Imagick($imgFile);
		$img->trimImage(0);
		$img->writeImage($imgFile);

		return $signature = [
			'signatureFile' => action('FilesController@getSignature', ['type' => 'forms', 'sub' => 'contract', 'filename' => $signatureFileName]),
			'signatureFileName' => $imgFile,
			'fileName' => $signatureFileName,
			'download' => action('FilesController@getSignature', ['type' => 'forms', 'sub' => 'contract', 'filename' => $signatureFileName])
		];
	}

	// Prepare data to send to API for saving
	private function getSaveData($data, $form)
	{
		$saveData['hash'] = $form->hash;
		$saveData['name'] = isset($data['name']) ? $data['name'] : null;
		$saveData['email'] = isset($data['email']) ? $data['email'] : null;

		$saveData['signature'] = isset($data['signature']) ? $data['signature'] : null;
		$saveData['date'] = isset($data['date']) ? $data['date'] : null;
		$saveData['ip'] = isset($data['ip']) ? $data['ip'] : null;
		$saveData['user_agent'] = isset($data['userAgent']) ? $data['userAgent'] : null;

		return $saveData;
	}

	/*
	*	Send PDF
	*/
	public function savePDF($data, $form)
	{
		$data['form'] = $form;

		// Set filename
		$now = str_replace([":"], "-", str_replace([" "], "_", Carbon::now()));
		$pdfFileName = preg_replace('|[^a-zA-Z_]|', '', $data['name']) .'_'. substr(md5(time() . rand(1,999)), 1, 4) . '_' . $now.'.pdf';

		// Create and save pdf
		$pdfFilePath = storage_path()."/forms/contract/pdf/".$pdfFileName;
		$pdf = PDF::loadView('contracts.pdf', $data)->save($pdfFilePath);
		$downloadPdfString = url("/pdf/forms/contract/".$pdfFileName);
		return $result = [
			'downloadPdf' => $downloadPdfString,
			'pdfFileName' => 'pdf/forms/contract/'.$pdfFileName,
			'fileName' => $pdfFileName
		];
	}

	private function notifySubmit($download, $shortData)
	{
		$resultJabberString = "New form was submitted. Details: \n";

		if (isset($shortData)) {
			$shortText = implode(";", $shortData);
			$resultJabberString = $resultJabberString.$shortText."\n";
		}

		if (isset($download['pdf'])) {
			$resultJabberString = $resultJabberString."Download the form in PDF: ".$download['pdf']."\n";
		}

		if (isset($download['signature'])) {
			$resultJabberString = $resultJabberString."Signature: ".$download['signature']."\n";
		}

		return $resultJabberString;

	}

}
