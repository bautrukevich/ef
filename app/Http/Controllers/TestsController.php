<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Session;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Contracts\Filesystem\Factory;

use App\Lib\Jabber\JabberSender;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use GuzzleHttp\Client;

class TestsController extends Controller {

	/*
	* Get data (API test)
	*/
	public function getData()
	{

		$apiKey = env('API_DATA_KEY');

		$url = url('api/data');

		$auth = 'X-Authorization: '.$apiKey;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLINFO_HEADER_OUT, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, Array($auth));

		$data = curl_exec($ch);

		curl_close($ch);

		//var_dump($data);

		// Response
		return $data;

	}

	/*
	* Send data (API test)
	*/
	public function sendData()
	{

		$client = new Client([
		    // Base URI is used with relative requests
		    'base_uri' => 'http://e-form-db.local/',
		    // You can set any number of default request options.
		    'timeout'  => 2.0,
		]);

		// $response = $client->request('POST', '/api', [
		//     'form_params' => [
		//         'foo' => 'bar',
		//         'baz' => ['hi', 'there!']
		//     ]
		// ]);
		//
		// return response()->json(json_decode($response->getBody()->getContents()));

		$response = $client->request('POST', '/api', [
		    'multipart' => [
				[
					'name'     => 'field_name',
					'contents' => 'abc'
				]
		        // [
		        //     'name'     => 'file_name',
		        //     'contents' => Storage::disk('local')->get('/oaf/files/OAF.png'),
		        // ]
		    ]
		]);

		return response()->json(json_decode($response->getBody()->getContents()));

		//fopen('/path/to/file', 'r')

		// dd(json_decode($response->getBody()->getContents()));
		//
		// dd(json_decode($response->getBody()));
		//
		// dd(json_decode($response->getBody()->getContents()));

	}

	/*
	* Destroy session
	*/
	public function sessionDestroy()
	{
		return Session::flush();
	}

	/*
	* Get all data in session
	*/
	public function sessionAll()
	{
		echo "<pre>";
		print_r(Session::all());
		echo "</pre>";
	}

	public function checkJabber()
	{
		$jabber = new JabberSender;
		$jabber->sendToAllJabbers("Hello everyone!");
	}

	public function getFiles()
	{
		$files = Storage::disk('local')->files('/oaf/files');
		$filesList = [];

		foreach ($files as $file) {

			$fileName = str_replace('oaf/files/', "", $file);

			$filesList['files'][] = [
				'fileName' => $fileName,
				'fileSize' => round((filesize(storage_path().'/'.$file))/1024, 2).' KB',
				'fileLink' => 'file/'.$fileName
			];
		}

		var_dump($filesList);

	}

}
