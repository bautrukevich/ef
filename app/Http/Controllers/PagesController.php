<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Lib\Form\FormHelper;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Contracts\Filesystem\Factory;

class PagesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('site.index');
	}

  /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function howItWorks()
	{
		return view('site.how-it-works');
	}

  /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function contacts()
	{
		return view('site.contacts');
	}

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function send(Request $request)
  {
    $formHelper = new FormHelper;

    $goodData = $formHelper->sanitaze([
      'Name' => $request->get('name'),
      'Company' => $request->get('company'),
      'Phone' => $request->get('phone'),
      'Email' => $request->get('email'),
      'Comments' => $request->get('comments'),
    ]);

    $resultString = "";
    foreach ($goodData as $key => $value) {
      $resultString .= $key.": ".$value."\n";
    }
    $resultString .= "\n";

    $disk = Storage::disk('local');
    $path = 'site/site_form.log';

    if ($disk->exists($path)) {
      $disk->append($path, $resultString);
    } else {
      $disk->put($path, $resultString);
    }

    return response()->json(['status' => 'success']);

  }
}
