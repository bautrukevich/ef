<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Contracts\Filesystem\Factory;

class FilesController extends Controller {

	/*
	*	Get generated PDF file
	*/
	public function getPdf($type, $sub, $filename)
	{
		$disk = Storage::disk('local');
		if ($disk->exists($type.'/'.$sub.'/pdf/'.$filename)) {
			return response()->download(storage_path().'/'.$type.'/'.$sub.'/pdf/'.$filename);
		} else {
			// Show 403 error
			abort(403);
		}
	}

	/*
	*	Get uploaded user file (resume or photo)
	*/
	public function getUserFile($type, $sub, $filename)
	{
		$disk = Storage::disk('local');
		if ($disk->exists($type.'/'.$sub.'/user_files/'.$filename)) {
			return response()->download(storage_path().'/'.$type.'/'.$sub.'/user_files/'.$filename);
		} else {
			// Show 403 error
			abort(403);
		}
	}

	/*
	*	Get files from oaf/files
	*/
	public function getFile($type, $sub, $filename)
	{
		$disk = Storage::disk('local');

		if ($disk->exists($type.'/'.$sub.'/files/'.$filename)) {
			return response()->download(storage_path().'/'.$type.'/'.$sub.'/files/'.$filename);
		} else {
			// Show 403 error
			abort(403);
		}
	}

	/*
	*	Get signature file
	*/
	public function getSignature($type, $sub, $filename)
	{
		$disk = Storage::disk('local');

		if ($disk->exists($type.'/'.$sub.'/signatures/'.$filename)) {
			return response()->download(storage_path().'/'.$type.'/'.$sub.'/signatures/'.$filename);
		} else {
			// Show 403 error
			abort(403);
		}
	}

}
