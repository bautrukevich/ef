<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Input;
use Session;

use App\Lib\EAF\FormClient;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AjaxController extends Controller {

	/**
	* Receive phone prefix by ISO
	*
	* @return Response
	*/
	public function getPhonePrefixByIso()
	{

		// Get query
		$iso = Input::get('iso');

		$data = json_decode(file_get_contents(base_path() . '/resources/prefixes.json'), true);

		if(isset($data[$iso])){
			return response()->json(['prefix' => '+' . strtr($data[$iso], ['+' => '']), 'status' => 'success']);
		} else {
			return response()->json(['prefix' => '+', 'status' => 'error']);
		}

	}

	/**
	* Send SMS using SMSC API
	*
	* @return Response
	*/
	public function getVerificationCode()
	{

		$phoneNumber = preg_replace('/[^\d]/','',Input::get('phone_number'));
		if(strlen($phoneNumber) < 10 || strlen($phoneNumber) > 15){
			return response()->json(['status' => 'error', 'message' => 'Failed send SMS. Wrong phone number']);
		}

		// Get country code and phone numebr
		// $countryCode = preg_replace('/[^\d]/','',Input::get('country_code'));
		$cellPhoneNumber = preg_replace('/[^\d]/','',Input::get('cell_phone_number'));

		Session::put('phone_number', '+'.$phoneNumber);
		// Session::put('country_code', '+'.$countryCode);
		Session::put('cell_phone_number', $cellPhoneNumber);

		if (Session::has('attempts')) {

			// Get codes left
			$attempts = intval(Session::get('attempts'));

			if($attempts < 1){
				// No attempts left
				return response()->json(['status' => 'error', 'message' => 'No attempts left.', 'left' => 0]);
			}

		} else {

			// Put atempts
			Session::put('attempts', 3);

			// Get codes left
			$attempts = intval(Session::get('attempts'));
		}

		$verificationCode = rand(1234, 9876);
		$verificationText = 'Your Verification Code: '.$verificationCode;
		Session::put('verification_code', $verificationCode);

		$client = new FormClient();
		$result = $client->sendSMS($phoneNumber, $verificationText);

		Session::put('attempts', $attempts - 1);
		return response()->json(['status' => 'success', 'message' => 'SMS was sent.', 'left' => ($attempts - 1), 'phone' => '+'.$phoneNumber, 'code' => $verificationCode]);

	}

	/**
	* Compare input code and verification code
	*
	* @return Response
	*/
	public function confirmVerificationCode()
	{
		// Get full cell phone number with country code
		$phoneNumber = preg_replace('/[^\d]/','',Input::get('phone_number'));

		// Get country code and phone numebr
		// $countryCode = preg_replace('/[^\d]/','',Input::get('country_code'));
		$cellPhoneNumber = preg_replace('/[^\d]/','',Input::get('cell_phone_number'));

		if(strlen($phoneNumber) < 10 || strlen($phoneNumber) > 15){
			return response()->json(['status' => 'error', 'message' => 'Wrong phone number!']);
		}

		$inputCode = preg_replace('/[^\d]/','',Input::get('code'));

		$verificationCode = Session::get('verification_code');

		$attempts = Session::get('attempts');

		Session::put('phone_number', '+'.$phoneNumber);
		// Session::put('country_code', '+'.$countryCode);
		Session::put('cell_phone_number', $cellPhoneNumber);

		if ($inputCode == $verificationCode) {
			Session::put('is_verificated', 'yes');
			return response()->json(['status' => 'success', 'message' => 'Phone was confirm', 'phone' => '+'.$phoneNumber]);
		} else {
			if ($attempts == 0) {

				if (Session::has('attempts_confirmed')) {

					// Get atempts left
					$attemptsConfirmed = intval(Session::get('attempts_confirmed'));
					$attemptsConfirmed = $attemptsConfirmed + 1;
					Session::put('attempts_confirmed', $attemptsConfirmed);

				} else {

					// Put atempts
					Session::put('attempts_confirmed', 1);

					// Get atempts left
					$attemptsConfirmed = intval(Session::get('attempts_confirmed'));
				}

			} else if ($attempts == 1) {
				Session::put('phone_number', '+'.$phoneNumber);
				// Session::put('country_code', '+'.$countryCode);
			}
			Session::put('is_verificated', 'no');
			return response()->json(['status' => 'error', 'message' => 'Wrong code!']);
		}

	}

	/**
	* Set verification failed
	*
	* @return Response
	*/
	public function setVerificationFailed()
	{
		Session::put('is_verificated', 'no');
		Session::put('is_failed', 'yes');
		return response()->json(['status' => 'success', 'message' => 'Phone was not confirm. Verification failed!']);
	}

	/**
	* Get status of verification phone
	*
	* @return Response
	*/
	public function getVerificationStatus()
	{

		if (Session::has('is_verificated')) {

			$status = Session::get('is_verificated');
			if ($status == 'yes') {
				return response()->json(['status' => 'success', 'message' => 'Phone was confirm']);
			} else if ($status == 'no') {
				return response()->json(['status' => 'error', 'message' => 'Phone was not confirm']);
			}

		} else {
			return response()->json(['status' => 'error', 'message' => 'Phone was not confirm']);
		}

	}

	public function getAttempts()
	{
		$attempts = Session::get('attempts');

		if (Session::has('attempts_confirmed')) {
			// Get atempts left
			$attemptsConfirmed = intval(Session::get('attempts_confirmed'));
		} else {
			// Put atempts
			Session::put('attempts_confirmed', 1);
			$attemptsConfirmed = intval(Session::get('attempts_confirmed'));
		}

		$attemptsMax = 5;

		if ($attempts == 0 && $attemptsConfirmed >= $attemptsMax) {
			return response()->json(['status' => 'success', 'confirmation' => 'failed', 'attemptsConfirmed' => $attemptsConfirmed, 'attempts' => $attempts]);
		} else {
			return response()->json(['status' => 'success', 'confirmation' => 'process', 'attemptsConfirmed' => $attemptsConfirmed, 'attempts' => $attempts]);
		}

	}

	public function getEmployeeData()
	{
		$email = Input::get('email');

		$client = new FormClient();
		$result = $client->getEmployeeData($email);

		if ($result) {
			return response()->json(['status' => 'success', 'attempts' => $result['data']]);
		} else {
			return response()->json(['status' => 'error', 'errors' => $result['errors']]);
		}
	}

}
