<?php namespace App;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class Question extends Model {

	protected $table = 'questions';

	protected $hidden = ['_token'];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'company', 'phone', 'email', 'comments'];

	public function getCreatedAtAttribute($value)
    {
		$createdAt = Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['created_at']);
		return Carbon::parse($createdAt)->format('M. d, Y, g:i a');
    }

}
