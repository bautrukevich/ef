<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Entry extends Model {

	protected $table = 'entries';

    protected $fillable = [
        'form_id',
        'email',
		'first_name',
		'middle_name',
		'last_name',
		'b_date',
		'cell_phone',
		'phone_is_confirmed',
		'linkedin',
		'facebook',
		'twitter',
		'institution',
		'degree',
		'major',
		'education_country',
		'education_start',
		'education_end',
		'education_description',
		'address_line_1',
		'address_line_2',
		'city',
		'region',
		'zip',
		'address_country',
		'employment_status',
		'employment_start',
		'occupation',
		'company',
		'experience_description',
		'salary',
		'employment_history_start',
		'employment_history_end',
		'signature',
		'date',
		'ip',
		'user_agent'
	];

	public function form()
    {
        return $this->belongsTo('App\Form', 'form_id');
    }

}
