<?php namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Lib\Markdown\MarkdownParser;

class Form extends Model {

	protected $table = 'forms';

	protected $hidden = ['_token'];

	protected $fillable = [
		'company',
		'name',
		'header',
		'tab_form',
		'block_info',
		'tab_info',
		'tab_info_content',
		'block_personal',
		'block_photo',
		'block_on_the_web',
		'block_education',
		'block_address',
		'block_hr_info',
		'block_experience',
		'block_resume',
		'block_signature',
		'mode_form_is_enabled',
		'mode_verify_phone',
		'mode_fake_sms',
		'mode_write_log_access',
		'mode_write_log_ip',
		'mode_send_mail',
		'mail_subject',
		'mail_from_name',
		'mail_to',
		'mail_to_name',
		'mode_xmpp_notify_submit',
		'mode_xmpp_notify_visit',
		'xmpp_accounts'
	];

	public function getTabFormContentAttribute($value)
    {
		$parser = new MarkdownParser(env('MARKDOWN_SYNTAX'));
		return $parser->parse($this->attributes['tab_form_content']);
    }

	public function getTabFormAgreementAttribute($value)
	{
		$parser = new MarkdownParser(env('MARKDOWN_SYNTAX'));
		return $parser->parse($this->attributes['tab_form_agreement']);
	}

	public function getTabInfoContentAttribute($value)
	{
		$parser = new MarkdownParser(env('MARKDOWN_SYNTAX'));
		return $parser->parse($this->attributes['tab_info_content']);
	}

	public function setNameAttribute($value)
    {
        $this->attributes['name'] = str_slug($value, "-");
    }

	public function setXmppAccountsAttribute($value)
    {
        $this->attributes['xmpp_accounts'] = json_encode($value);
    }

	public function entries()
    {
        return $this->hasMany('App\Entry', 'form_id');
    }

}
