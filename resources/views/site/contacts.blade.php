@extends('site')

@section('header')

  <!-- # HEADER -->
  <div class="header header-white header-padding units-row end">

    <div class="unit-100 unit-max unit-centered">

      <div class="units-row end">
        <div class="unit-30 end">
          <div class="hide-on-mobile">
            <a class="h2 logo-link logo logo-text upper" href="/">{{ env('COMPANY_NAME') }}</a>
          </div>
          <div class="navigation-toggle end" data-tools="navigation-toggle" data-target="#js-main-menu">
            <span class="end text-bold upper">{{ env('COMPANY_NAME') }}</span>
          </div>
        </div>
        <div class="unit-70 end">
          <div id="js-main-menu" class="unit-100 unit-centered end">

            <header class="group">
              <nav class="navbar navbar-top small text-bold upper end navbar-right navbar-mobile">
                <ul>
                  <li>
                    <a class="text-centered-mobile menu-item menu-item-top" href="{{ url('/') }}">Overview</a>
                  </li>
                  <li>
                    <a class="text-centered-mobile menu-item menu-item-top" href="{{ url('how-it-works') }}">How it works</a>
                  </li>
                  <li>
                    <span class="text-centered-mobile menu-item menu-item-top">Contacts</span>
                  </li>
                  <li>
                    &nbsp;&nbsp;
                  </li>
                  <li>
                    <i class="feather-icon-head"></i>&nbsp;
                    <a class="text-centered-mobile menu-item menu-item-top" href="javascript:void(0);" id="show-modal" data-tools="modal" data-width="500" data-title="Login" data-content="#js-login">Login</a>
                  </li>
                </ul>
              </nav>
            </header>

          </div>
        </div>
      </div>

    </div>

  </div>
  <!-- # END HEADER -->

@stop

@section('wrapper')

  <!-- # CONTENT -->
	<div class="content sticky-content">

		<!-- # ABOUT -->
		<div class="about units-row end">

			<div class="unit-100 unit-max unit-centered">

				<div class="units-row">
					<div class="unit-90 unit-centered block-padding">

            @if(env('APP_MAP_&_CONTACTS'))

						<div class="units-row">
							<div class="unit-50">

								<h1 class="pause text-bold">Contact Us</h1>

								<p class="pause">
									Call us:
									<br>
									<span class="big text-bold"><?php echo getenv('PHONE'); ?></span>
									<br>
								</p>

								<p class="pause">
									Or write us:
									<br>
									<a class="link big text-bold" href="mailto:<?php echo getenv('EMAIL'); ?>"><?php echo getenv('EMAIL'); ?></a>
								</p>
								<p>
									Our location:
									<br>
									<i class="svg-icon svg-icon-map svg-icon-map-big"></i><span class="big text-bold"><?php echo getenv('ADDRESS'); ?></span>
								</p>

							</div>

              <div class="unit-50 end">
    						<!-- # MAP -->
    						<div class="units-row end map hide-on-mobile">

    							<div class="js-map" id="google-map" style="height: 100%;" data-lat="<?php echo getenv('MAP_MARKER_CENTER_X'); ?>" data-lng="<?php echo getenv('MAP_MARKER_CENTER_Y'); ?>" data-hint="<?php echo getenv('COMPANY_NAME'); ?>" data-zoom="<?php echo getenv('MAP_ZOOM'); ?>"></div>

    						</div>
    						<!-- # END MAP -->
    					</div>

						</div>

            <br>

            @endif

						<div class="js-form-submit-success hide">
							<h1 class="pause text-bold end color-green">
								<i class="feather feather-icon-circle-check color-green"></i>&nbsp;Thank you for your message.
								<br class="hide-on-desktop">
								<span class="color-black small">We will answer you as soon as possible.</span>
							</h1>
						</div>

						<div class="js-form-submit-error hide">
							<h1 class="pause text-bold end color-red">
								<i class="feather feather-icon-circle-cross color-red"></i>&nbsp;Ooops. Something going wrong
								<br class="hide-on-desktop">
								<span class="color-black small">Please try again later.</span>
							</h1>
						</div>

						<div class="js-application-form">
							<h1 class="pause text-bold">Have Questions?</h1>

							<div class="units-row end">

                {!! Form::open(['class' => 'js-app-form forms', 'method' => 'POST', 'action' => ['PagesController@send'], 'data-parsley-validate']); !!}

									<div class="units-row end">

										<div class="unit-50">
											<label>
												Name:
												<input type="text" name="name" class="js-app-name width-100" data-parsley-required data-parsley-required-message="Please enter your name">
											</label>
											<label>
												Company:
												<input type="text" name="company" class="js-app-company width-100">
											</label>
											<label>
												Phone:
												<input type="text" name="phone" class="js-app-phone width-100" data-parsley-required data-parsley-required-message="Please enter your phone">
											</label>
											<label>
												E-mail:
												<input type="text" name="email" class="js-app-email width-100" data-parsley-required data-parsley-type="email" data-parsley-required-message="Please enter your e-mail" data-parsley-email-message="Please enter a valid e-mail">
											</label>
										</div>

										<div class="unit-50">
											<label>
												Comments:
												<textarea class="js-app-comments" name="comments" rows="11" cols="40" data-parsley-required data-parsley-required-message="Please enter your comments"></textarea>
											</label>
											<p>
												<input class="js-send-form btn btn-blue btn-big big width-100" value="Send" type="submit" name="submit">
											</p>
										</div>

									</div>

                {!! Form::close(); !!}

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>
		<!-- # END ABOUT -->

	</div>
	<!-- # END CONTENT -->

@stop

@section('footer')

  <!-- # FOOTER -->
  <div class="footer units-row end">

    <div class="unit-100 unit-max unit-centered">

      <div class="units-row end">
        <div class="unit-90 unit-centered block-padding">

          <div class="units-row end">
            <div class="unit-30 end unit-role-right">

              <div class="copyright">
                <p class="color-black-light pause">&copy;
                  <span class="text-bold upper">{{ env('COMPANY_NAME') }}</span>, 2015-2016</p>
              </div>

            </div>
            <div class="unit-70 end unit-role-left footer-menu hide-on-mobile">
              <div id="js-second-menu" class="unit-100 unit-centered end">

                <header class="group">
                  <nav class="navbar navbar-footer small text-bold upper end navbar-left navbar-mobile">
                    <ul>
                      <li>
                        <a class="text-centered-mobile menu-item" href="{{ url('/') }}">Overview</a>
                      </li>
                      <li>
                        <a class="text-centered-mobile menu-item" href="{{ url('how-it-works') }}">How it works</a>
                      </li>
                      <li>
                        <span class="text-centered-mobile menu-item">Contacts</span>
                      </li>
                    </ul>
                  </nav>
                </header>

              </div>
            </div>
          </div>

        </div>
      </div>

    </div>

  </div>
  <!-- # END FOOTER -->

@stop

@section('map')
  <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBtUHxdRAHOnMp4QVXksPuPsrR0R3Qup8w&callback=initMap">
  </script>
@stop
