@extends('site')

@section('header')

  <!-- # HEADER -->
  <div class="header header-white header-padding units-row end">

    <div class="unit-100 unit-max unit-centered">

      <div class="units-row end">
        <div class="unit-30 end">
          <div class="hide-on-mobile">
            <a class="h2 logo-link logo logo-text upper" href="/">{{ env('COMPANY_NAME') }}</a>
          </div>
          <div class="navigation-toggle end" data-tools="navigation-toggle" data-target="#js-main-menu">
            <span class="end text-bold upper">{{ env('COMPANY_NAME') }}</span>
          </div>
        </div>
        <div class="unit-70 end">
          <div id="js-main-menu" class="unit-100 unit-centered end">

            <header class="group">
              <nav class="navbar navbar-top small text-bold upper end navbar-right navbar-mobile">
                <ul>
                  <li>
                    <a class="text-centered-mobile menu-item menu-item-top" href="{{ url('/') }}">Overview</a>
                  </li>
                  <li>
                    <span class="text-centered-mobile menu-item menu-item-top">How it works</span>
                  </li>
                  <li>
                    <a class="text-centered-mobile menu-item menu-item-top" href="{{ url('contacts') }}">Contacts</a>
                  </li>
                  <li>
                    &nbsp;&nbsp;
                  </li>
                  <li>
                    <i class="feather-icon-head"></i>&nbsp;
                    <a class="text-centered-mobile menu-item menu-item-top" href="javascript:void(0);" id="show-modal" data-tools="modal" data-width="500" data-title="Login" data-content="#js-login">Login</a>
                  </li>
                </ul>
              </nav>
            </header>

          </div>
        </div>
      </div>

    </div>

  </div>
  <!-- # END HEADER -->

@stop

@section('wrapper')

  <!-- # CONTENT -->
	<div class="content sticky-content">

		<div class="block-background block-background-white units-row">

			<div class="unit-100 unit-max unit-centered">

				<div class="units-row block-padding-sides">

					<div class="unit-100">

						<div class="units-row end">
							<div class="unit-60 unit-centered">

								<div class="how-it-works">
										<svg viewBox="0 0 40 39" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
											<!-- Generator: Sketch 3.3 (11970) - http://www.bohemiancoding.com/sketch -->
											<title>hiw</title>
											<desc>Created with Sketch.</desc>
											<defs></defs>
											<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
												<g id="hiw" sketch:type="MSLayerGroup" stroke="#2575ED" fill="#FFFFFF">
													<path d="M5.7,28.5 C3.7,30.4 2.2,31.6 2.2,31.6 C0.7,33.2 0.6,35.6 2,37 C3.4,38.4 5.8,38.4 7.3,36.9 C7.4,36.8 8.7,35.6 10.6,33.6 L5.7,28.5 L5.7,28.5 Z M5,35.5 C4.2,35.5 3.5,34.8 3.5,34 C3.5,33.2 4.2,32.5 5,32.5 C5.8,32.5 6.5,33.2 6.5,34 C6.5,34.8 5.8,35.5 5,35.5 L5,35.5 Z"
													id="Shape" sketch:type="MSShapeGroup"></path>
													<g id="Group" transform="translate(0.000000, 6.000000)" sketch:type="MSShapeGroup">
														<path d="M16.0805734,25.687742 L16.1423529,25.6423529 L16.1423529,25.6423529 L16.8,26.3 C17.9,27.4 19.8,27.4 21,26.2 C22.2,25 22.2,23.2 21.1,22 L20.45,21.35 L20.5110622,21.2889378 L20.5110622,21.2889378 L22.4,23.4 C23.5,24.5 25.4,24.5 26.6,23.3 C27.8,22.1 27.8,20.3 26.7,19.1 C26.7,19.1 25.7603119,18.3502488 24.4372195,17.3627805 L24.7,17.1 L25.8,18.2 C26.6,19 28.7,19.5 30.1,18.1 C31.5,16.7 30.7,14.7 30,13.9 C29.3,13.2 26.8,10.7 26.8,10.7 C26.8,10.7 28.2,6.5 27.9,4.3 C27.8,2.4 26.7,1.1 24.4,0.8 L19.4,0 C15.2,0 11.6,0.5 8.6,3.5 L3.7,8.4 C0.2,12 -7.77156117e-16,17.9 3.6,21.5 C3.6,21.5 4.46115054,22.3611505 5.86478231,23.7647823 C5.96192174,23.9165528 6.07371141,24.0622306 6.2,24.2 L11.3,29.3 C12.4,30.4 14.3,30.4 15.5,29.2 C16.4873661,28.2126339 16.6623223,26.819063 16.0805734,25.687742 Z"
														id="Shape"></path>
													</g>
													<g id="Group" transform="translate(23.000000, 0.000000)" sketch:type="MSShapeGroup">
														<path d="M15.5,6.5 L11,11 C11,11 9.3,11 7.6,9.4 C6,7.7 6,6 6,6 L10.6,1.4 C7.3,0.4 4.1,1.2 1.8,3.5 C-1.7,7 1.4,11.4 0.5,12.3 L-3.55271368e-15,12.8 L4.2,17 L4.8,16.4 C5.7,15.5 10,18.8 13.6,15.2 C15.7,12.9 16.6,10.1 15.5,6.5 L15.5,6.5 Z" id="Shape"></path>
													</g>
												</g>
											</g>
										</svg>
								</div>
								<h1 class="text-centered pause text-bold">How it works</h1>
								<br>
							</div>
						</div>

						<div class="units-row">
							<div class="unit-25">
								<span class="step-number right text-centered hide-on-mobile">1</span>
							</div>
							<div class="unit-60 step-block">
								<h2>
									<span class="hide-on-desktop-inline">
										<span class="step-number right text-centered">1</span>
									</span>
									Companies come to us in search of ideal candidates
								</h2>
								<p class="pause">(from referrals or direct contact)</p>
							</div>
						</div>

						<div class="units-row">
							<div class="unit-25">
								<span class="step-number right text-centered hide-on-mobile">2</span>
							</div>
							<div class="unit-60 step-block">
								<h2>
									<span class="hide-on-desktop-inline">
										<span class="step-number right text-centered">2</span>&nbsp;&nbsp;</span>We find the right experienced people</h2>
								<p class="pause">(from referrals, schools, job fairs, job boards, labor exchange offices, and so on)</p>
							</div>
						</div>

						<div class="units-row">
							<div class="unit-25">
								<span class="step-number right text-centered hide-on-mobile">3</span>
							</div>
							<div class="unit-60 step-block">
								<h2>
									<span class="hide-on-desktop-inline">
										<span class="step-number right text-centered">3</span>&nbsp;&nbsp;</span>We get in touch with the candidates with the job offer</h2>
								<p class="pause">(using email, phone, or VoIP)</p>
							</div>
						</div>

						<div class="units-row">
							<div class="unit-25">
								<span class="step-number right text-centered hide-on-mobile">4</span>
							</div>
							<div class="unit-60 step-block end">
								<h2>
									<span class="hide-on-desktop-inline">
										<span class="step-number right text-centered">4</span>&nbsp;&nbsp;</span>We perform screenings of the prospective candidates to find the one who perfectly fits the job</h2>
								<p class="pause">(mainly using proprietary employment platforms as well as through phone, email, and face-to-face meetings)</p>
							</div>
						</div>

						<div class="units-row">
							<div class="unit-25">
								<span class="step-number right text-centered hide-on-mobile">5</span>
							</div>
							<div class="unit-60 step-block pause">
								<h2>
									<span class="hide-on-desktop-inline">
										<span class="step-number right text-centered">5</span>&nbsp;&nbsp;</span>We get you acquainted with the best fits</h2>
							</div>
						</div>

						<div class="units-row">
							<div class="unit-25">
								<span class="step-number right text-centered hide-on-mobile">6</span>
							</div>
							<div class="unit-60 step-block end">
								<h2>
									<span class="hide-on-desktop-inline">
										<span class="step-number right text-centered">6</span>&nbsp;&nbsp;</span>You call them for an online or phone interview and decide</h2>
								<p class="pause">(and as soon as the interview has been successfully carried out, your ideal employee will be ready to get down to work)</p>
							</div>
						</div>

					</div>

				</div>

			</div>

		</div>

	</div>
	<!-- # END CONTENT -->

@stop

@section('footer')

  <!-- # FOOTER -->
  <div class="footer units-row end">

    <div class="unit-100 unit-max unit-centered">

      <div class="units-row end">
        <div class="unit-90 unit-centered block-padding">

          <div class="units-row end">
            <div class="unit-30 end unit-role-right">

              <div class="copyright">
                <p class="color-black-light pause">&copy;
                  <span class="text-bold upper">{{ env('COMPANY_NAME') }}</span>, 2015-2016</p>
              </div>

            </div>
            <div class="unit-70 end unit-role-left footer-menu hide-on-mobile">
              <div id="js-second-menu" class="unit-100 unit-centered end">

                <header class="group">
                  <nav class="navbar navbar-footer small text-bold upper end navbar-left navbar-mobile">
                    <ul>
                      <li>
                        <a class="text-centered-mobile menu-item" href="{{ url('/') }}">Overview</a>
                      </li>
                      <li>
                        <span class="text-centered-mobile menu-item">How it works</span>
                      </li>
                      <li>
                        <a class="text-centered-mobile menu-item" href="{{ url('contacts') }}">Contacts</a>
                      </li>
                    </ul>
                  </nav>
                </header>

              </div>
            </div>
          </div>

        </div>
      </div>

    </div>

  </div>
  <!-- # END FOOTER -->

@stop
