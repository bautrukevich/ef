@extends('site')

@section('header')

  <!-- # HEADER -->
  <div class="header header-white header-padding units-row end">

    <div class="unit-100 unit-max unit-centered">

      <div class="units-row end">
        <div class="unit-30 end">
          <div class="hide-on-mobile">
            <span class="h2 logo-link logo logo-text upper">{{ env('COMPANY_NAME') }}</span>
          </div>
          <div class="navigation-toggle end" data-tools="navigation-toggle" data-target="#js-main-menu">
            <span class="end text-bold upper">{{ env('COMPANY_NAME') }}</span>
          </div>
        </div>
        <div class="unit-70 end">
          <div id="js-main-menu" class="unit-100 unit-centered end">

            <header class="group">
              <nav class="navbar navbar-top small text-bold upper end navbar-right navbar-mobile">
                <ul>
                  <li>
                    <span class="text-centered-mobile menu-item menu-item-top">Overview</span>
                  </li>
                  <li>
                    <a class="text-centered-mobile menu-item menu-item-top" href="{{ url('how-it-works') }}">How it works</a>
                  </li>
                  <li>
                    <a class="text-centered-mobile menu-item menu-item-top" href="{{ url('contacts') }}">Contacts</a>
                  </li>
                  <li>
                    &nbsp;&nbsp;
                  </li>
                  <li>
                    <i class="feather-icon-head"></i>&nbsp;
                    <a class="text-centered-mobile menu-item menu-item-top" href="javascript:void(0);" id="show-modal" data-tools="modal" data-width="500" data-title="Login" data-content="#js-login">Login</a>
                  </li>
                </ul>
              </nav>
            </header>

          </div>
        </div>
      </div>

    </div>

  </div>
  <!-- # END HEADER -->

@stop

@section('wrapper')

  <!-- # CONTENT -->
  <div class="content sticky-content">

    <!-- # ABOUT -->
    <div class="about-us units-row end">

      <div class="back back-blue"></div>

      <div class="unit-100 unit-max unit-centered">

        <div class="units-row">
          <div class="unit-80 unit-centered block-padding text-centered about-us-padding">

            <h1 class="text-bold color-white">About us</h1>

            <p class="lead end"><?php echo getenv('COMPANY_NAME'); ?> was established because its founders got really tired of looking for a professional staffing company and decided to show the world what an efficient and solid staffing business should look like. We are a team of skilled
              industry experts with the right expertise who are eager to help decent companies and willing jobseekers find each other.</p>

          </div>
        </div>

      </div>

    </div>
    <!-- # END ABOUT -->

    <!-- # ABOUT -->
    <div class="units-row end">

      <div class="unit-100 unit-max unit-centered">

        <div class="units-row">
          <div class="unit-90 unit-centered block-padding block-padding-main">

            <h1 class="text-bold">What we do</h1>

            <br>

            <h3 class="text-bold">Why us?</h3>

            <p class="lead">Only a sales-driven and cost-efficient business can reach the pinnacle of the business world's heights. And that means hiring the right people at the right time. We work with both small and large companies and are ready to tackle your staffing challenges
              any time so that your business can soar.</p>

          </div>
        </div>

      </div>

    </div>
    <!-- # END ABOUT -->

    <div class="block-background block-background-blue units-row">

      <div class="unit-100 unit-max unit-centered">

        <div class="units-row end">
          <div class="unit-90 unit-centered block-padding">

            <h1 class="text-bold text-centered">Our Benefits</h1>

            <br>

            <div class="units-row end">
              <div class="unit-33 text-centered">

                <div class="simple-icon simple-icon-know">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="48" height="39.938" viewBox="0 0 48 39.938">
                      <defs>
                        <style>
                          .cls-1 {
                            fill: #2575ed;
                            fill-rule: evenodd;
                          }
                        </style>
                      </defs>
                      <path d="M48.000,30.948 C48.000,32.601 46.654,33.945 45.000,33.945 C43.346,33.945 42.000,32.601 42.000,30.948 C42.000,29.648 42.839,28.549 44.000,28.136 L44.000,15.719 L36.959,19.874 C36.974,19.942 37.000,20.004 37.000,20.076 L37.000,36.058 C37.000,36.594 36.575,37.023 36.045,37.048 C34.597,37.777 29.742,39.938 23.000,39.938 C16.257,39.938 11.402,37.777 9.955,37.047 C9.425,37.022 9.000,36.594 9.000,36.058 L9.000,20.076 C9.000,20.004 9.026,19.941 9.041,19.874 L0.491,14.828 C0.187,14.647 0.000,14.320 0.000,13.967 C0.000,13.614 0.187,13.287 0.491,13.107 L22.492,0.121 C22.806,-0.064 23.196,-0.064 23.510,0.121 L45.510,13.107 C45.510,13.107 45.511,13.108 45.511,13.108 C45.913,13.168 46.000,13.448 46.000,13.938 L46.000,13.964 C46.000,13.965 46.001,13.966 46.001,13.967 C46.001,13.969 46.000,13.970 46.000,13.971 L46.000,28.136 C47.161,28.549 48.000,29.648 48.000,30.948 ZM35.000,35.332 L35.000,21.031 L23.510,27.813 C23.353,27.906 23.177,27.952 23.001,27.952 C22.825,27.952 22.649,27.906 22.492,27.813 L11.000,21.030 L11.000,35.333 C12.452,36.034 16.950,37.940 23.000,37.940 C29.049,37.940 33.548,36.034 35.000,35.332 ZM23.001,2.142 L2.966,13.967 L23.001,25.793 L43.035,13.967 L23.001,2.142 ZM45.010,29.951 C44.950,29.986 44.883,30.023 44.870,30.029 C44.863,30.034 44.819,30.004 44.792,29.991 C44.344,30.090 44.000,30.471 44.000,30.948 C44.000,31.499 44.449,31.947 45.000,31.947 C45.551,31.947 46.000,31.499 46.000,30.948 C46.000,30.401 45.557,29.957 45.010,29.951 Z"
                      class="cls-1" />
                    </svg>


                </div>

                <p class="lead pause text-bold">Field-specific services </p>
                <p class="pause">— We employ field-specific professionals in dozens of industries who possess extensive knowledge of the industry standards in order to ensure excellence and precision in uniting the best companies with the right people.</p>
              </div>
              <div class="unit-33 text-centered">

                <div class="simple-icon simple-icon-like">
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="44" height="47" viewBox="0 0 44 46.844">
                    <defs>
                      <style>
                        .cls-1 {
                          fill: #2575ed;
                          fill-rule: evenodd;
                        }
                      </style>
                    </defs>
                    <path d="M31.978,46.839 L22.984,46.839 C19.625,46.839 17.224,45.935 15.105,45.137 C13.316,44.463 11.771,43.882 9.890,43.882 C6.921,43.882 3.985,43.848 3.985,43.848 C3.826,43.846 3.670,43.806 3.530,43.732 C3.168,43.542 -0.001,41.646 -0.001,33.877 C-0.001,26.311 2.038,22.579 2.125,22.423 C2.266,22.170 2.511,21.991 2.796,21.933 C2.869,21.918 10.108,20.387 14.283,16.222 C18.230,12.283 20.623,6.050 20.946,1.929 C21.038,0.744 22.117,-0.167 23.332,0.018 C25.029,0.271 28.980,1.646 28.980,8.950 C28.980,12.216 28.540,15.902 28.000,17.844 L37.000,17.844 C40.654,17.844 42.970,19.655 42.970,22.442 C42.970,23.854 42.457,24.862 41.771,25.559 C43.064,26.266 44.001,27.558 44.001,29.515 C44.001,31.455 43.058,32.643 41.823,33.280 C42.483,33.985 42.970,34.999 42.970,36.412 C42.970,38.549 41.854,39.788 40.452,40.393 C40.765,40.901 40.972,41.558 40.972,42.394 C40.972,46.152 37.942,46.839 31.978,46.839 ZM4.323,41.858 C5.155,41.865 7.511,41.888 9.890,41.888 C12.135,41.888 13.921,42.560 15.811,43.271 C17.865,44.046 19.989,44.845 22.984,44.845 L31.978,44.845 C38.973,44.845 38.973,43.710 38.973,42.394 C38.973,41.017 37.974,40.864 37.859,40.850 C37.324,40.789 36.933,40.314 36.977,39.778 C37.021,39.242 37.467,38.844 38.023,38.863 C38.024,38.863 38.025,38.863 38.027,38.863 C38.955,38.863 40.972,38.622 40.972,36.412 C40.972,34.138 38.946,33.880 38.859,33.870 C38.319,33.809 37.928,33.328 37.978,32.788 C38.028,32.249 38.492,31.849 39.044,31.886 L39.174,31.888 C40.230,31.888 42.002,31.580 42.002,29.515 C42.002,27.152 39.940,26.897 39.054,26.897 C38.526,26.897 38.049,26.488 38.016,25.962 C37.983,25.436 38.325,24.976 38.849,24.911 C39.197,24.862 40.972,24.507 40.972,22.442 C40.972,20.164 38.180,19.844 37.000,19.844 L26.000,19.844 C25.681,19.844 25.571,19.765 25.383,19.508 C25.195,19.250 25.142,18.918 25.239,18.615 C25.257,18.561 26.981,13.172 26.981,8.950 C26.981,4.840 25.581,2.369 23.037,1.991 C23.000,1.986 22.943,2.025 22.939,2.083 C22.598,6.432 20.186,13.152 15.696,17.632 C11.707,21.611 5.490,23.341 3.691,23.774 C3.223,24.847 1.998,28.197 1.998,33.877 C1.998,39.435 3.770,41.384 4.323,41.858 Z"
                    class="cls-1" />
                  </svg>


                </div>

                <p class="lead pause text-bold">Only handpicked
                  <br>and professional staff</p>
                <p class="pause">– All-out screening, checking, and assessment procedures carried out by our expert HR department ensures a consistent flow of candidates who would make a perfect fit for any position at your company.</p>
              </div>
              <div class="unit-33 text-centered end">

                <div class="simple-icon simple-icon-rocket">
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="48" height="48" viewBox="0 0 47.625 48">
                    <defs>
                      <style>
                        .cls-1 {
                          fill: #2575ed;
                          fill-rule: evenodd;
                        }
                      </style>
                    </defs>
                    <path d="M43.878,21.997 C37.413,28.416 32.216,31.108 26.777,33.820 L26.777,40.866 C26.777,41.098 26.697,41.324 26.550,41.502 L21.522,47.635 C21.333,47.867 21.051,47.998 20.758,47.998 C20.698,47.998 20.637,47.992 20.576,47.981 C20.221,47.914 19.930,47.659 19.817,47.314 L16.847,38.338 L9.572,31.007 L0.665,28.014 C0.323,27.899 0.070,27.606 0.003,27.249 C-0.063,26.891 0.069,26.526 0.347,26.294 L6.432,21.229 C6.609,21.081 6.833,21.000 7.064,21.000 L14.055,21.000 C16.746,15.518 19.419,10.280 25.788,3.766 C28.831,0.653 35.565,-0.000 40.679,-0.000 C43.929,-0.000 46.365,0.258 46.467,0.269 C46.932,0.318 47.299,0.688 47.348,1.157 C47.416,1.800 48.952,16.957 43.878,21.997 ZM45.451,2.186 C44.486,2.109 42.738,2.000 40.679,2.000 C33.986,2.000 29.200,3.126 27.201,5.171 C20.841,11.675 18.276,16.906 15.561,22.443 C15.393,22.784 15.050,23.000 14.672,23.000 L7.421,23.000 L2.991,26.687 L10.421,29.184 C10.568,29.233 10.701,29.316 10.809,29.426 L18.416,37.091 C18.524,37.201 18.607,37.335 18.655,37.482 L21.133,44.971 L24.793,40.506 L24.793,33.199 C24.793,32.818 25.007,32.471 25.345,32.302 C30.840,29.567 36.029,26.982 42.484,20.573 C45.917,17.164 45.834,6.830 45.451,2.186 ZM32.731,21.000 C29.448,21.000 26.777,18.309 26.777,15.000 C26.777,11.692 29.448,9.001 32.731,9.001 C36.014,9.001 38.685,11.692 38.685,15.000 C38.685,18.309 36.014,21.000 32.731,21.000 ZM32.731,11.001 C30.542,11.001 28.762,12.794 28.762,15.000 C28.762,17.206 30.542,19.000 32.731,19.000 C34.920,19.000 36.700,17.206 36.700,15.000 C36.700,12.794 34.920,11.001 32.731,11.001 ZM8.523,35.032 C9.052,34.892 9.595,35.210 9.734,35.745 C9.874,36.278 9.557,36.825 9.027,36.965 C8.012,37.236 5.088,38.224 3.784,39.537 C2.836,40.493 2.262,43.653 2.046,45.989 C4.610,45.919 7.896,45.487 8.793,44.585 C10.060,43.307 10.752,40.119 10.911,38.999 C10.989,38.455 11.491,38.076 12.034,38.152 C12.576,38.230 12.953,38.736 12.876,39.282 C12.808,39.761 12.158,44.021 10.196,45.999 C8.363,47.845 3.051,48.000 1.315,48.000 C1.173,48.000 1.055,47.999 0.965,47.998 C0.696,47.994 0.439,47.880 0.255,47.683 C0.071,47.485 -0.026,47.219 -0.013,46.947 C0.022,46.257 0.384,40.137 2.381,38.123 C4.334,36.156 8.354,35.077 8.523,35.032 Z"
                    class="cls-1" />
                  </svg>


                </div>

                <p class="lead pause text-bold">The quickest response</p>
                <p class="pause">– We use a large and constantly updated pool of qualified candidates so that we can find and hire the most skilled people for you immediately upon your first request.</p>
              </div>
            </div>

          </div>
        </div>

      </div>

    </div>

    <!-- # ABOUT -->
    <div class="units-row end">

      <div class="unit-100 unit-max unit-centered">

        <div class="units-row">
          <div class="unit-90 unit-centered block-padding">

            <h2 class="text-bold">How We Work</h2>

            <p class="big">There are three hiring options for you to choose from:</p>

            <div class="units-row">
              <div class="unit-33">
                <p class="indent lead pause"><i class="feather feather-icon-circle-check color-blue"></i>&nbsp;Contractual employment is when we provide an employee who is going to work for your company under a contract.</p>
              </div>
              <div class="unit-33">
                <p class="indent lead pause"><i class="feather feather-icon-circle-check color-blue"></i>&nbsp;Contract-to-hire is almost the same as usual contractual employment, however with one key difference. Once the contract has expired, you will be able to choose whether to recruit
                  the employee or not.</p>
              </div>
              <div class="unit-33">
                <p class="indent lead end"><i class="feather feather-icon-circle-check color-blue"></i>&nbsp;Direct hire is when we provide an employee who is going to work at a permanent position in your company. This option does not involve any middle party engaged in the hiring process.</p>
              </div>
            </div>

          </div>
        </div>

      </div>

    </div>
    <!-- # END ABOUT -->

    <!-- # ABOUT -->
    <div class="block-background block-background-blue units-row">

      <div class="unit-100 unit-max unit-centered">

        <div class="units-row end">
          <div class="unit-90 unit-centered block-padding">

            <h1 class="text-bold text-centered">Sit Back and Watch While We Do the Job</h1>

            <br>


            <p class="pause">We are often told that recently it has become harder and harder to find employees who are qualified and skilled enough both professionally and personally to fit perfectly into a company's structure. That's why many companies come to us for high-quality
              labor. It's our primary duty to help you find professionals who will make a solid contribution to your company's growth.</p>

            <p class="pause">We offer a wide array of sound staffing services which include pre-screening, checking, assessment, and orientation activities. We work hard to provide you with quality labor and make it easy for you to grow fast.</p>
            <br>
            <p class="lead end">Thanks to our unique business approach and proprietary labor management techniques we can spare you time and money, working forwards to achieve lower expenses and higher efficiency.</p>

          </div>
        </div>

      </div>

    </div>
    <!-- # END ABOUT -->

    <!-- # ABOUT -->
    <div class="units-row end">

      <div class="unit-100 unit-max unit-centered">

        <div class="units-row">
          <div class="unit-90 unit-centered block-padding">

            <h2 class="text-bold">Industries We Work In</h2>

            <div class="tags">
              <span class="tag">Accounting</span>
              <span class="tag">Administrative</span>
              <span class="tag">Architectural Design</span>
              <span class="tag">Aviation</span>
              <span class="tag">Automotive</span>
              <span class="tag">CallCenter</span>
              <span class="tag">Medicine</span>
              <span class="tag">Construction</span>
              <span class="tag">Engineering</span>
              <span class="tag">Energy</span>
              <span class="tag">Environmental</span>
              <span class="tag">Labor</span>
              <span class="tag">Manufacturing</span>
              <span class="tag">Mortgage</span>
              <span class="tag">Science</span>
            </div>

          </div>
        </div>

      </div>

    </div>
    <!-- # END ABOUT -->

  </div>
  <!-- # END CONTENT -->

@stop

@section('footer')

  <!-- # FOOTER -->
  <div class="footer units-row end">

    <div class="unit-100 unit-max unit-centered">

      <div class="units-row end">
        <div class="unit-90 unit-centered block-padding">

          <div class="units-row end">
            <div class="unit-30 end unit-role-right">

              <div class="copyright">
                <p class="color-black-light pause">&copy;
                  <span class="text-bold upper">{{ env('COMPANY_NAME') }}</span>, 2015-2016</p>
              </div>

            </div>
            <div class="unit-70 end unit-role-left footer-menu hide-on-mobile">
              <div id="js-second-menu" class="unit-100 unit-centered end">

                <header class="group">
                  <nav class="navbar navbar-footer small text-bold upper end navbar-left navbar-mobile">
                    <ul>
                      <li>
                        <span class="text-centered-mobile menu-item">Overview</span>
                      </li>
                      <li>
                        <a class="text-centered-mobile menu-item" href="{{ url('how-it-works') }}">How it works</a>
                      </li>
                      <li>
                        <a class="text-centered-mobile menu-item" href="{{ url('contacts') }}">Contacts</a>
                      </li>
                    </ul>
                  </nav>
                </header>

              </div>
            </div>
          </div>

        </div>
      </div>

    </div>

  </div>
  <!-- # END FOOTER -->

@stop
