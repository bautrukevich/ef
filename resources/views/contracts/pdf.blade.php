@extends('pdf')

@section('wrapper')

	<!-- # MAIN -->
	<div class="wrapper">

		<div class="units-row end">

			<div class="unit-100">

				<table class="table-flat">
					<tr>
						<td class="text-left" width="50" valign="top">
							<h2 class="up end">{{ $form->company->name }}</h2>
						</td>
						<td class="text-right" width="50" valign="top">
							<p class="up end">
								Address: {{ $form->company->address }}<br>
								Tel: {{ $form->company->phone_number }}; Fax: {{ $form->company->fax_number }}
							</p>
						</td>
					</tr>
				</table>

			</div>

		</div>

		<div class="units-row end">

			<div class="unit-100 unit-centered end">

				<h4>{{ $form->header }}</h4>

				<div class="markdown-body pause">
					{!! $form->tab_form_content !!}
				</div>

				<br>

				<table class="table-flat table-stripped end">

					<tr>
						<td colspan="2">
							<h4 class="end">COMPANY INFO</h4>
						</td>
					</tr>

					<tr width="40">
						<td>Company name</td>
						<td>{{ $form->company->name }}</td>
					</tr>
					<tr>
						<td>Business register number</td>
						<td>{{ $form->company->business_register_number }}</td>
					</tr>
					<tr>
						<td>Legal address</td>
						<td>{{ $form->company->address }}</td>
					</tr>
					<tr>
						<td>CEO</td>
						<td>{{ $form->company->ceo }}</td>
					</tr>

					<tr>

						<td valign="middle">Signature</td>
						<td valign="middle">

						<img src="{{ $form->company->signature }}" alt="Signature" height="40"> / {{ $form->company->ceo }} /

						</td>

					</tr>

					<tr>
						<td colspan="2" style="background-color: #fff;">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="2" style="background-color: #fff;">&nbsp;</td>
					</tr>

					<tr>
						<td colspan="2">
							<h4 class="end">EMPLOYEE INFO</h4>
						</td>
					</tr>

					<tr width="40">
						<td>Employee name</td>
						<td>{{ isset($name) ? $name : '———' }}</td>
					</tr>
					<tr>
						<td>Employee email</td>
						<td>{{ isset($email) ? $email : '———' }}</td>
					</tr>

					<tr>
						<td>Date</td>
						<td>{{ isset($date) ? $date : '———' }}</td>
					</tr>
					<tr>
						<td>Detected IP</td>
						<td>{{ isset($ip) ? $ip : '———' }}</td>
					</tr>
					<tr>

						<td valign="middle">Employee signature</td>
						<td valign="middle">

						@if (isset($signatureFile))

							<img src="{{ $signatureFile }}" alt="Signature" height="40"> / {{ isset($name) ? $name : '———' }} /

						@else

							{{ '_______________' }} / {{ isset($name) ? $name : '———' }} /

						@endif

						</td>

					</tr>
				</table>

			</div>

		</div>

	</div>
	<!-- # END MAIN -->

@stop
