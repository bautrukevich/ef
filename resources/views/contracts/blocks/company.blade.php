<h2 class="background-gray">Company info</h2>

<!-- Company name -->
<div class="units-row">

    <div class="unit-25 end">

        <label class="end">
            Company name
        </label>

    </div>
    <div class="unit-50 end">

        <label class="end">
            <b>{{ $form->company->name }}</b>
        </label>

    </div>

</div>

<!-- Business register number -->
<div class="units-row">

    <div class="unit-25 end">

        <label class="end">
            Business register number
        </label>

    </div>
    <div class="unit-50 end">

        <label class="end">
            <b>{{ $form->company->business_register_number }}</b>
        </label>

    </div>

</div>

<!-- Legal address -->
<div class="units-row">

    <div class="unit-25 end">

        <label class="end">
            Legal address
        </label>

    </div>
    <div class="unit-50 end">

        <label class="end">
            <b>{{ $form->company->address }}</b>
        </label>

    </div>

</div>

<!-- CEO -->
<div class="units-row">

    <div class="unit-25 end">

        <label class="end">
            CEO
        </label>

    </div>
    <div class="unit-50 end">

        <label class="end">
            <b>{{ $form->company->ceo }}</b>
        </label>

    </div>

</div>

<!-- CEO -->
<div class="units-row">

    <div class="unit-25 end">

        <label class="end">
            Signature
        </label>

    </div>
    <div class="unit-50 end">

        <label class="end">
            {{-- <b>{{ $form->company->signature }}</b> --}}
            <img src="{{ $form->company->signature }}" style="max-height: 50px;" />
        </label>

    </div>

</div>

<br>
