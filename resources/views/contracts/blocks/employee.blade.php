<h2 class="background-gray">Employee info</h2>

<!-- Employee email -->
<div class="units-row">

    <div class="unit-25 end">

        <label class="end" for="js-employee-email">
            Employee email <span class="req">*</span>
        </label>

    </div>
    <div class="unit-50 end">

        <div class="field">
            <input id="js-employee-email" type="text" name="email" class="width-100" data-parsley-required data-parsley-required-message="Please enter e-mail" data-parsley-type="email" data-parsley-email-message="Please enter correct e-mail" data-parsley-trigger="change">
        </div>

        <div class="units-row end">
            <div class="unit-100">
                <div class="forms-desc">Please use email from application form</div>
            </div>
        </div>

    </div>

</div>

<!-- Employee name -->
<div class="units-row">

    <div class="unit-25 end">

        <label class="end" for="js-employee-name">
            Employee name <span class="req">*</span>
        </label>

    </div>
    <div class="unit-50 end">

        <div class="field">
            <input id="js-employee-name js-readonly" type="text" name="name" class="js-capitalize caps width-100" data-parsley-required data-parsley-required-message="Please enter employee name">
        </div>

    </div>

</div>

<br>
