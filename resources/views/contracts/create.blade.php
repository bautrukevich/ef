@extends('app')

@section('header')

	<!-- # HEADER -->
	<div class="header units-row">

		<div class="unit-100 unit-max unit-centered">

			<p class="color-white logo end">{{ $form->company->name }}</p>

		</div>

	</div>
	<!-- # END HEADER -->

@stop

@section('wrapper')

	<!-- # MAIN -->
	<div class="wrapper">

		<div class="units-row">

			<div class="form-wrapper unit-100 unit-max unit-centered">

				<p class="color-green"><i class="feather-icon-lock"></i> Encrypted сonnection</p>

				<h1 class="form-header">{{ $form->header }}</h1>

				<div id="js-tab-form">

					<div class="js-application-form">

						<div class="markdown-body pause">
							{!! $form->tab_form_content !!}
						</div>

						{!! Form::open(['class' => 'js-form-submit forms', 'method' => 'POST', 'action' => ['ContractsController@store', $id], 'files' => true, 'data-prefix' => '', 'data-parsley-validate', 'data-parsley-excluded' => 'input[type=button], input[type=submit],  input[type=reset], [disabled]']); !!}

						{{-- {!! Form::open(['class' => 'forms', 'method' => 'POST', 'action' => ['ContractsController@store', $id], 'files' => true, 'data-prefix' => '']); !!} --}}

							@include('contracts.blocks.company')

							@include('contracts.blocks.employee')

							@include('contracts.blocks.signature')

							<!-- Submit form -->
							<div class="units-row">

								<div class="unit-65 unit-push-25">

									<p>
										<button class="js-btn-submit btn btn-blue btn-big width-100">
											<div class="js-btn-submit-spinner ui active mini inline loader inverted js-hide"></div>
											<span id="btn-text">Submit form</span>
										</button>
									</p>

								</div>

							</div>

						{!! Form::close(); !!}

					</div>

					<div class="js-form-submit-success units-row hide">

						<div class="unit-100 unit-max unit-centered">

							<div class="markdown-body pause">{!! $form->thank_you_text !!}</div>

							@if ($form->mode_form_output == 'pdf')

								<div class="units-row">

									<div class="unit-50">

										<p class="pause">1. Download your form PDF print it and sign</p>

										<p>
											<a class="js-pdf-link pdf-link btn btn-blue big" href="#" target="_blank">Download PDF</a>
										</p>

									</div>

									<div class="unit-50">

										<p class="pause">2. Send the signed form by fax number:</p>

										<div class="block-small block-bordered-gray">
											<p class="end">{{ $form->company->fax_number }}</p>
										</div>

									</div>

								</div>

							@endif

							<br>
							<p class="pause">
								<a class="btn btn-blue btn-outline" href="{{ action('ContractsController@create', ['id' => $form->hash]) }}">Go back</a>
							</p>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>
	<!-- # END MAIN -->

@stop
