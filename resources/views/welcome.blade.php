<!DOCTYPE html>
<html>

	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

		<!-- Suppress IE6's pop-up-on-mouseover toolbar for images, that can interfere with certain designs. -->
		<meta http-equiv="imagetoolbar" content="false" />
		<meta name="description" content="HR PROCESS">

		<title>HR PROCESS</title>

		<link href="{{ url('assets/site/css/style.css') }}" rel="stylesheet">

		<!-- Direct search spiders to your sitemap. -->
		<link rel="sitemap" type="application/xml" title="Sitemap" href="#">

	</head>

	<body class="sticky-body">
	<!--[if lt IE 9]>
		<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->

    	<!-- # HEADER -->
    	<div class="header header-white header-padding units-row end">

    		<div class="unit-100 unit-max unit-centered">

    			<div class="units-row end">
    				<div class="unit-100 end text-centered">
    					<span class="h2 logo-link logo logo-text" href="index.html">HR PROCESS</span>
    				</div>
    			</div>

    		</div>

    	</div>
    	<!-- # END HEADER -->

    	<!-- # CONTENT -->
    	<div class="content sticky-content">

        <!-- # ABOUT -->
        <div class="about-us units-row end">
        	<div class="back back-blue"></div>
        	<div class="unit-100 unit-max unit-centered">
        		<div class="units-row">
        			<div class="unit-100 unit-centered block-padding">
        				<br><br><br>
        				<div class="units-row">

        					<div class="unit-50 unit-centered">

        						<div class="units-row cap">
        							<div class="unit-100 block-padding">

        								<div class="forms">
        									<div class="js-errors hide">
        										<p class="error pause">Please check your login or password!</p>
        									</div>
        									<!-- Login -->
        									<div class="units-row pause">
        										<div class="unit-100 end">
        											<label class="end" for="js-first-name">
        												E-mail
        												<input id="js-email" type="text" name="email" class="width-100">
        											</label>
        										</div>
        									</div>
        									<!-- Password -->
        									<div class="units-row pause">
        										<div class="unit-100 end">
        											<label for="js-phone-number">
        												Password
        												<input id="js-password" type="text" name="password" class="width-100">
        											</label>
        										</div>
        									</div>
        									<div class="units-row end">
        										<div class="unit-100 end">
        											<button class="js-btn-submit btn btn-blue btn-big width-100"><span id="spinner"></span> <span id="btn-text"><i class="svg-icon svg-icon-phone"></i>Login</span></button>
        										</div>
        									</div>

        								</div>
        							</div>
        						</div>

        					</div>
        				</div>

        			</div>
        		</div>
        	</div>
        </div>
        <!-- # END ABOUT -->

    	</div>
    	<!-- # END CONTENT -->

		<script src="{{ url('assets/site/js/all.js') }}"></script>

	</body>

</html>
