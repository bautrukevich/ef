<!DOCTYPE html>
<html>

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

    <!-- Suppress IE6's pop-up-on-mouseover toolbar for images, that can interfere with certain designs. -->
    <meta http-equiv="imagetoolbar" content="false" />

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>{{ env('APP_COMPANY') }}</title>

    <link href="{{ url('/assets/welcome/css/style.css') }}" rel="stylesheet">

  </head>

  <body class="sticky-body">
    <!--[if lt IE 9]>
      <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    @yield('header')

    @yield('wrapper')

    @yield('footer')

    <!-- # MODALS -->
    <div id="js-login" class="hide">

          <div class="forms">

              <div class="js-errors hide">
                  <p class="error pause">Please check your login or password!</p>
              </div>

              <!-- Login -->
              <div class="units-row pause">

                  <div class="unit-100 end">

                      <label class="end" for="js-first-name">
                          E-mail
                          <input id="js-email" type="text" name="email" class="width-100">
                      </label>

                  </div>

              </div>

              <!-- Password -->
              <div class="units-row pause">

                  <div class="unit-100 end">

                      <label for="js-phone-number">
                          Password
                          <input id="js-password" type="text" name="password" class="width-100">
                      </label>

                  </div>

              </div>

              <div class="units-row end">

                  <div class="unit-100 end">

                      <button class="js-btn-submit btn btn-blue btn-big width-100">
                          <span id="spinner"></span>
                          <span id="btn-text"><i class="svg-icon svg-icon-phone"></i>Login</span>
                      </button>

                  </div>

              </div>

          </div>

    </div>
    <!-- # END MODALS -->

    <script src="{{ url('assets/welcome/js/all.js') }}"></script>

    @yield('map')

  </body>

</html>
