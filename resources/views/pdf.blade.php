<!DOCTYPE html>
<html>

	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

		<!-- Suppress IE6's pop-up-on-mouseover toolbar for images, that can interfere with certain designs. -->
		<meta http-equiv="imagetoolbar" content="false" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

		<meta name="csrf-token" content="{{ csrf_token() }}" />

		<title>Employment application</title>

		<link href='http://fonts.googleapis.com/css?family=Roboto&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
		<link href="{{ url('/assets/sys/css/pdf.css') }}" rel="stylesheet">

	</head>

	<body>
	<!--[if lt IE 9]>
		<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->

	@yield('header')

	@yield('wrapper')

	@yield('footer')

	<script src="{{ url('assets/sys/js/main.js') }}"></script>

</body>

</html>
