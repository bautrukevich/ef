<!-- Cell Phone Confirmation-->
@if ($verifyPhone['isVerificated'])

<div class="js-confirm-success units-row">

    <div class="block-gray phone-verification unit-65 unit-push-25 end alert alert-success">
        <h5 class="upper"><i class="feather-icon-circle-check"></i> Cell Phone confirmation</h5>
        Your phone <b class="js-phone-confirmed">{{ $verifyPhone['phoneNumber'] }}</b> was successfully confirmed!
    </div>
    <br><br>

</div>

@elseif ($verifyPhone['isFailed'])

<div class="js-confirm-failed units-row">

    <div class="block-gray phone-verification unit-65 unit-push-25 end alert alert-error">
        <h5 class="upper"><i class="feather-icon-circle-cross"></i> Cell Phone confirmation</h5>
        Your phone <b class="js-phone-confirmed">{{ $verifyPhone['phoneNumber'] }}</b> was not confirmed!
    </div>
    <br><br>

</div>

@else

<div class="js-phone-confirm units-row">

    <div class="block-gray phone-verification unit-65 unit-push-25 end">

        <h5 class="upper">Cell Phone confirmation</h5>

        <div class="js-confirm-block">

            <div class="js-form-code group">

                <p class="pause">Every application form must be confirmed by verifying your mobile number.<br>You will receive an SMS with a 4-digit code to your mobile by pressing “Receive code” button.</p>

                @if ($verifyPhone['attemptsNumber'] > 0)

                <button class="js-btn-receive btn-receive btn btn-blue left pause">
                    <div class="js-btn-receive-spinner ui active mini inline inverted loader js-hide"></div>
                    <span class="js-btn-receive-text">Receive code</span>
                </button>

                @endif

                @if ($verifyPhone['attemptsNumber'] < 3)
                <div class="js-verification left field pause">
                @else
                <div class="js-verification left field hide pause">
                @endif
                    <span class="btn-group">

                        @if ($verifyPhone['attemptsNumber'] == 0)

                        <input class="verification-code left" id="js-verification-code" type="text" name="verificationCode" maxlength="4" placeholder="Enter code" data-parsley-errors-container="#js-code-verification-errors" data-parsley-group="js-block-code" data-parsley-trigger="change">

                        @else

                        <input class="verification-code left" id="js-verification-code" type="text" name="verificationCode" data-parsley-required data-parsley-required-message="Please enter code" maxlength="4" placeholder="Enter code" data-parsley-errors-container="#js-code-verification-errors" data-parsley-group="js-block-code" data-parsley-trigger="change">

                        @endif

                        @if ($verifyPhone['attemptsNumber'] < 3)
                        <button class="js-confirm-btn verification-code-confirm left btn btn-gray btn-outline end">
                            <div class="js-btn-confirm-spinner ui active mini inline loader js-hide"></div>
                            <span class="js-btn-confirm-text">Confirm</span>
                        </button>
                        @else
                        <button class="js-confirm-btn verification-code-confirm left btn btn-gray btn-outline end" disabled="disabled">
                            <div class="js-btn-confirm-spinner ui active mini inline loader js-hide"></div>
                            <span class="js-btn-confirm-text">Confirm</span>
                        </button>
                        @endif

                    </span>
                    <div id="js-phone-verification-errors" class="error small"></div>
                    <div id="js-code-verification-errors" class="error small"></div>
                </div>

            </div>

            <p class="js-sms-sent-success color-green end hide">SMS was successfully sent!</p>
            <p class="js-sms-sent-failed color-red end hide">SMS was not sent. Please check your phone number!</p>
            <p class="js-sms-sent-error color-red end hide">SMS was not sent. Something going wrong. Please, check your internet connection and try later</p>

            @if ($verifyPhone['attemptsNumber'] < 3)

                @if ($verifyPhone['attemptsNumber'] == 0)
                <p class="js-attempts-text end">
                    You haven't attempts to receive SMS.
                </p>
                @else
                <p class="js-attempts-text end">
                    <span class="js-attempts">{{ $verifyPhone['attempts'] }}</span> left. Next try in <span class="js-seconds">30 seconds</span>.
                </p>
                @endif

            @else

            <p class="js-attempts-text hide end">
                <span class="js-attempts">3 attempts</span> left. Next try in <span class="js-seconds">30 seconds</span>.
            </p>

            @endif

        </div>

    </div>

</div>

<div class="js-confirm-success units-row hide">

    <div class="block-gray phone-verification unit-65 unit-push-25 end alert alert-success">
        <h5 class="upper"><i class="feather-icon-circle-check"></i> Cell Phone confirmation</h5>
        Your phone <b class="js-phone-confirmed"></b> was successfully confirmed!
    </div>
    <br><br>

</div>

<div class="js-confirm-failed units-row hide">

    <div class="block-gray phone-verification unit-65 unit-push-25 end alert alert-error">
        <h5 class="upper"><i class="feather-icon-circle-cross"></i> Cell Phone confirmation</h5>
        Your phone <b class="js-phone-confirmed"></b> was not confirmed!
    </div>
    <br><br>

</div>

@endif
