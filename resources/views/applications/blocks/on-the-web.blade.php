<h2 class="background-gray">On the web</h2>

<!-- LinkedIn -->
<div class="units-row">

    <div class="unit-25 end">

        <label class="end" for="js-linkedin">
            LinkedIn
        </label>

    </div>
    <div class="unit-50 end">

        <div class="field">
            <input id="js-linkedin" type="text" name="linkedin" class="width-100" />
        </div>

    </div>

</div>

<!-- Facebook -->
<div class="units-row">

    <div class="unit-25 end">

        <label class="end" for="js-facebook">
            Facebook
        </label>

    </div>
    <div class="unit-50 end">

        <div class="field">
            <input id="js-facebook" type="text" name="facebook" class="width-100" />
        </div>

    </div>

</div>

<br>
