<h2 class="background-gray">Resume</h2>

<!-- File upload -->
<div class="units-row">

    <div class="unit-25 end">

        <label class="end" for="js-file-upload">
            Resume
        </label>

    </div>
    <div class="unit-50 end">

        <div class="js-fileinput fileinput-new" data-provides="fileinput">
            <span class="btn btn-gray btn-file btn-outline">
                <span class="fileinput-new">Select file</span>
                <span class="fileinput-exists">Change</span>
                <input id="js-file-type-resume" type="file" name="resume" data-parsley-pattern="/([а-яА-Яa-zA-Z0-9\s_\\.\-:])+(.pdf|.doc|.docx|.txt|.jpg|.jpeg|.bmp|.png)$/" data-parsley-pattern-message="The file must be a file of type: jpg, jpeg, bmp, png, pdf, doc, docx, txt." data-parsley-errors-container="#js-resume-errors" data-parsley-trigger="change">
            </span>
            <span class="fileinput-filename"></span>
            <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
        </div>
        <div id="js-resume-errors"></div>
        <div class="field">
            <div class="js-resume-file-types forms-desc">The file must be a file of type: jpg, jpeg, bmp, png, pdf, doc, docx, txt.</div>
        </div>

    </div>

</div>

<br>
