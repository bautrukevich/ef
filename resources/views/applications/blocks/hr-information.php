<h2 class="background-gray">HR information</h2>

<!-- Employment status -->
<div class="units-row">

    <div class="unit-25 end">

        <label class="end" for="js-empoyment-status">
            Employment status <span class="req">*</span>
        </label>

    </div>
    <div class="unit-50 end">

        <div class="field">
            <select id="js-empoyment-status" name="employmentStatus" data-parsley-required data-parsley-required-message="Please choose empoyment status">
                <option value="">Choose</option>
                <option value="Employed">Employed</option>
                <option value="Unemployed">Unemployed</option>
                <option value="Active Job Search">Active Job Search</option>
                <option value="Retired">Retired</option>
            </select>
        </div>

    </div>

</div>

<!-- Employment start -->
<div class="units-row">

    <div class="unit-25 end">

        <label class="end" for="js-employment-start">
            When would you be able to start new <span class="nowrap">employment? <span class="req">*</span></span>
        </label>

    </div>
    <div class="unit-50 end">

        <div class="field">
            <select id="js-employment-start" name="employmentStart" data-parsley-required data-parsley-required-message="Please choose empoyment start">
                <option value="">Choose</option>
                <option value="Immediately">Immediately</option>
                <option value="In 1 week">In 1 week</option>
                <option value="In 2 weeks">In 2 weeks</option>
            </select>
        </div>

    </div>

</div>

<!-- Preferred job type -->
<div class="units-row">

    <div class="unit-25 end">

        <label class="end" for="js-preferred-job-type">
            Preferred job type <span class="req">*</span>
        </label>

    </div>
    <div class="unit-50 end">

        <div class="field">
            <select id="js-preferred-job-type" name="preferredJobType" data-parsley-required data-parsley-required-message="Please choose preferred job type">
                <option value="">Choose</option>
                <option value="Full Time 9-17">Full Time 9-17</option>
                <option value="Part Time 9-13">Part Time 9-13</option>
                <option value="Part Time 14-18">Part Time 14-18</option>
            </select>
        </div>

    </div>

</div>

<!-- Best time to call -->
<div class="units-row">

    <div class="unit-25 end">

        <label class="end" for="js-best-time">
            Best time to call <span class="req">*</span>
        </label>

    </div>
    <div class="unit-50 end">

        <div class="field">
            <select id="js-best-time" name="bestTimeToCall" data-parsley-required data-parsley-required-message="Please choose best time to call">
                <option value="">Choose</option>
                <option value="Anytime">Anytime</option>
                <option value="9:00 AM - 11:00 AM">9:00 AM - 11:00 AM</option>
                <option value="11:00 AM - 1:00 PM">11:00 AM - 1:00 PM</option>
                <option value="1:00 PM - 3:00 PM">1:00 PM - 3:00 PM</option>
                <option value="3:00 PM - 5:00 PM">3:00 PM - 5:00 PM</option>
                <option value="5:00 PM - 7:00 PM">5:00 PM - 7:00 PM</option>
            </select>
        </div>

    </div>

</div>

<br>
