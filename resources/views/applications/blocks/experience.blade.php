<h2 class="background-gray">Experience<br><span class="small color-gray-dark light">(Present or most recent employer)</span></h2>

<!-- Occupation / Title -->
<div class="units-row">

    <div class="unit-25 end">

        <label class="end" for="js-occupation">
            Occupation / Title <span class="req">*</span>
        </label>

    </div>
    <div class="unit-50 end">

        <div class="field">
            <input id="js-occupation" type="text" name="occupation" class="js-capitalize caps width-100" data-parsley-required data-parsley-required-message="Please enter occupation / title">
        </div>

    </div>

</div>

<!-- Company / Business name -->
<div class="units-row">

    <div class="unit-25 end">

        <label class="end" for="js-company">
            Company / Business name <span class="req">*</span>
        </label>

    </div>
    <div class="unit-50 end">

        <div class="field">
            <input id="js-company" type="text" name="company" class="js-capitalize caps width-100" data-parsley-required data-parsley-required-message="Please enter company / business name">
        </div>

    </div>

</div>

<!-- Description -->
<div class="units-row">

    <div class="unit-25 end">

        <label class="end" for="js-experience-description">
            Description <span class="req">*</span>
        </label>

    </div>
    <div class="unit-65 end">

        <div class="field">
            <textarea id="js-experience-description" name="experienceDescription" class="js-capitalize caps width-100" data-parsley-required data-parsley-required-message="Please enter experience description"></textarea>
        </div>

    </div>

</div>

<!-- Salary -->
<div class="units-row">

    <div class="unit-25 end">

        <label class="end" for="js-salary">
            Salary <span class="req">*</span>
        </label>

    </div>
    <div class="unit-50 end">

        <div class="field">
            <input id="js-salary" type="text" name="salary" class="js-capitalize caps width-100" data-parsley-required data-parsley-required-message="Please enter salary">
        </div>

    </div>

</div>

<!-- Start of employment -->
<div class="units-row">

    <div class="unit-25 end">

        <label class="end" for="js-employment-month-start">
            Start of employment <span class="req">*</span>
        </label>

    </div>
    <div class="unit-50 end">

        <div class="units-row end">
            <div class="unit-75">
                <div class="field">
                    <select id="js-employment-month-start" name="employmentMonthStart" data-parsley-required data-parsley-required-message="Please choose month">
                        <option value="">Month</option>
                        <option value="January">January</option>
                        <option value="February">February</option>
                        <option value="March">March</option>
                        <option value="April">April</option>
                        <option value="May">May</option>
                        <option value="June">June</option>
                        <option value="July">July</option>
                        <option value="August">August</option>
                        <option value="September">September</option>
                        <option value="October">October</option>
                        <option value="November">November</option>
                        <option value="December">December</option>
                        <option value="January">January</option>
                    </select>
                </div>
            </div>
            <div class="unit-25 end">
                <div class="field">
                    <select id="js-employment-year-start" name="employmentYearStart"  data-parsley-required data-parsley-required-message="Choose year">
                        <option value="">Year</option>
                        <option value="2016">2016</option>
                        <option value="2015">2015</option>
                        <option value="2014">2014</option>
                        <option value="2013">2013</option>
                        <option value="2012">2012</option>
                        <option value="2011">2011</option>
                        <option value="2010">2010</option>
                        <option value="2009">2009</option>
                        <option value="2008">2008</option>
                        <option value="2007">2007</option>
                        <option value="2006">2006</option>
                        <option value="2005">2005</option>
                        <option value="2004">2004</option>
                        <option value="2003">2003</option>
                        <option value="2002">2002</option>
                        <option value="2001">2001</option>
                        <option value="2000">2000</option>
                        <option value="1999">1999</option>
                        <option value="1998">1998</option>
                        <option value="1997">1997</option>
                        <option value="1996">1996</option>
                        <option value="1995">1995</option>
                        <option value="1994">1994</option>
                        <option value="1993">1993</option>
                        <option value="1992">1992</option>
                        <option value="1991">1991</option>
                        <option value="1990">1990</option>
                        <option value="1989">1989</option>
                        <option value="1988">1988</option>
                        <option value="1987">1987</option>
                        <option value="1986">1986</option>
                        <option value="1985">1985</option>
                        <option value="1984">1984</option>
                        <option value="1983">1983</option>
                        <option value="1982">1982</option>
                        <option value="1981">1981</option>
                    </select>
                </div>
            </div>
        </div>

    </div>

</div>

<!-- End of employment -->
<div class="units-row">

    <div class="unit-25 end">

        <label class="end" for="js-employment-month-end">
            End of employment
        </label>
        <input class="js-employment-end" type="hidden">

    </div>
    <div class="unit-50 end">

        <div class="js-employment units-row pause">
            <div class="unit-75">
                <div class="field">
                    <select id="js-employment-month-end" name="employmentMonthEnd">
                        <option value="">Month</option>
                        <option value="January">January</option>
                        <option value="February">February</option>
                        <option value="March">March</option>
                        <option value="April">April</option>
                        <option value="May">May</option>
                        <option value="June">June</option>
                        <option value="July">July</option>
                        <option value="August">August</option>
                        <option value="September">September</option>
                        <option value="October">October</option>
                        <option value="November">November</option>
                        <option value="December">December</option>
                        <option value="January">January</option>
                    </select>
                </div>
            </div>
            <div class="unit-25 end">
                <div class="field">
                    <select id="js-employment-year-end" name="employmentYearEnd">
                        <option value="">Year</option>
                        <option value="2016">2016</option>
                        <option value="2015">2015</option>
                        <option value="2014">2014</option>
                        <option value="2013">2013</option>
                        <option value="2012">2012</option>
                        <option value="2011">2011</option>
                        <option value="2010">2010</option>
                        <option value="2009">2009</option>
                        <option value="2008">2008</option>
                        <option value="2007">2007</option>
                        <option value="2006">2006</option>
                        <option value="2005">2005</option>
                        <option value="2004">2004</option>
                        <option value="2003">2003</option>
                        <option value="2002">2002</option>
                        <option value="2001">2001</option>
                        <option value="2000">2000</option>
                        <option value="1999">1999</option>
                        <option value="1998">1998</option>
                        <option value="1997">1997</option>
                        <option value="1996">1996</option>
                        <option value="1995">1995</option>
                        <option value="1994">1994</option>
                        <option value="1993">1993</option>
                        <option value="1992">1992</option>
                        <option value="1991">1991</option>
                        <option value="1990">1990</option>
                        <option value="1989">1989</option>
                        <option value="1988">1988</option>
                        <option value="1987">1987</option>
                        <option value="1986">1986</option>
                        <option value="1985">1985</option>
                        <option value="1984">1984</option>
                        <option value="1983">1983</option>
                        <option value="1982">1982</option>
                        <option value="1981">1981</option>
                    </select>
                </div>
            </div>
        </div>

        <label class="end">
            <input class="js-current-work" type="checkbox" name="currentWork" value="Present"> I currently work here
        </label>

    </div>

</div>

<br>
