<h2 class="background-gray">Personal</h2>

@if ($form->block_photo)
<!-- Photo -->
<div class="units-row">

    <div class="unit-25 end">

        <label class="end" for="js-picture-upload">
            Add picture
        </label>

    </div>
    <div class="unit-50 end">

        <div class="js-fileinput fileinput-new" data-provides="fileinput">
            <span class="btn btn-gray btn-file btn-outline">
                <span class="fileinput-new">Select file</span>
                <span class="fileinput-exists">Change</span>
                <input id="js-file-type-photo" type="file" name="photo" data-parsley-pattern="/([а-яА-Яa-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.bmp|.png)$/" data-parsley-pattern-message="The file must be a file of type: jpg, jpeg, bmp, png." data-parsley-errors-container="#js-photo-errors" data-parsley-trigger="change">
            </span>
            <span class="fileinput-filename"></span>
            <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
        </div>
        <div id="js-photo-errors"></div>
        <div class="field">
            <div class="js-photo-file-types forms-desc">The file must be a file of type: jpg, jpeg, bmp, png.</div>
        </div>

    </div>

</div>
@endif

<!-- First Name -->
<div class="units-row">

    <div class="unit-25 end">

        <label class="end" for="js-first-name">
            First Name <span class="req">*</span>
        </label>

    </div>
    <div class="unit-50 end">

        <div class="field">
            <input id="js-first-name" type="text" name="firstName" class="js-capitalize caps width-100" data-parsley-required data-parsley-required-message="Please enter your first name">
        </div>

    </div>

</div>

<!-- Middle Name -->
<div class="units-row">

    <div class="unit-25 end">

        <label class="end" for="js-middle-name">
            Middle Name
        </label>

    </div>
    <div class="unit-50 end">

        <div class="field">
            <input id="js-middle-name" type="text" name="middleName" class="js-capitalize caps width-100" />
        </div>

    </div>

</div>

<!-- Last Name -->
<div class="units-row">

    <div class="unit-25 end">

        <label class="end" for="js-last-name">
            Last Name <span class="req">*</span>
        </label>

    </div>
    <div class="unit-50 end">

        <div class="field">
            <input id="js-last-name" type="text" name="lastName" class="js-capitalize caps width-100" data-parsley-required data-parsley-required-message="Please enter your last name">
        </div>

    </div>

</div>

<!-- Date Of Birth -->
<div class="units-row">

    <div class="unit-25 end">

        <label class="end" for="js-b-month">
            Date Of Birth <span class="req">*</span>
        </label>

    </div>
    <div class="unit-50 end">

        <div class="units-row end">
            <div class="unit-50">
                <div class="field">
                    <select id="js-b-month" name="bMonth" data-parsley-required data-parsley-required-message="Please choose month">
                        <option value="">Month</option>
                        <option value="January">January</option>
                        <option value="February">February</option>
                        <option value="March">March</option>
                        <option value="April">April</option>
                        <option value="May">May</option>
                        <option value="June">June</option>
                        <option value="July">July</option>
                        <option value="August">August</option>
                        <option value="September">September</option>
                        <option value="October">October</option>
                        <option value="November">November</option>
                        <option value="December">December</option>
                        <option value="January">January</option>
                    </select>
                </div>
            </div>
            <div class="unit-25">
                <div class="field">
                    <select id="js-b-day" name="bDay" data-parsley-required data-parsley-required-message="Choose day">
                        <option value="">Day</option>
                        <option value="01">01</option>
                        <option value="02">02</option>
                        <option value="03">03</option>
                        <option value="04">04</option>
                        <option value="05">05</option>
                        <option value="06">06</option>
                        <option value="07">07</option>
                        <option value="08">08</option>
                        <option value="09">09</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                        <option value="13">13</option>
                        <option value="14">14</option>
                        <option value="15">15</option>
                        <option value="16">16</option>
                        <option value="17">17</option>
                        <option value="18">18</option>
                        <option value="19">19</option>
                        <option value="20">20</option>
                        <option value="21">21</option>
                        <option value="22">22</option>
                        <option value="23">23</option>
                        <option value="24">24</option>
                        <option value="25">25</option>
                        <option value="26">26</option>
                        <option value="27">27</option>
                        <option value="28">28</option>
                        <option value="29">29</option>
                        <option value="30">30</option>
                        <option value="31">31</option>
                    </select>
                </div>
            </div>
            <div class="unit-25 end">
                <div class="field">
                    <select id="js-b-year" name="bYear" data-parsley-required data-parsley-required-message="Choose year">
                        <option value="">Year</option>
                        <option value="2000">2000</option>
                        <option value="1999">1999</option>
                        <option value="1998">1998</option>
                        <option value="1997">1997</option>
                        <option value="1996">1996</option>
                        <option value="1995">1995</option>
                        <option value="1994">1994</option>
                        <option value="1993">1993</option>
                        <option value="1992">1992</option>
                        <option value="1991">1991</option>
                        <option value="1990">1990</option>
                        <option value="1989">1989</option>
                        <option value="1988">1988</option>
                        <option value="1987">1987</option>
                        <option value="1986">1986</option>
                        <option value="1985">1985</option>
                        <option value="1984">1984</option>
                        <option value="1983">1983</option>
                        <option value="1982">1982</option>
                        <option value="1981">1981</option>
                        <option value="1980">1980</option>
                        <option value="1979">1979</option>
                        <option value="1978">1978</option>
                        <option value="1977">1977</option>
                        <option value="1976">1976</option>
                        <option value="1975">1975</option>
                        <option value="1974">1974</option>
                        <option value="1973">1973</option>
                        <option value="1972">1972</option>
                        <option value="1971">1971</option>
                        <option value="1970">1970</option>
                        <option value="1969">1969</option>
                        <option value="1968">1968</option>
                        <option value="1967">1967</option>
                        <option value="1966">1966</option>
                        <option value="1965">1965</option>
                        <option value="1964">1964</option>
                        <option value="1963">1963</option>
                        <option value="1962">1962</option>
                        <option value="1961">1961</option>
                        <option value="1960">1960</option>
                        <option value="1959">1959</option>
                        <option value="1958">1958</option>
                        <option value="1957">1957</option>
                        <option value="1956">1956</option>
                        <option value="1955">1955</option>
                        <option value="1954">1954</option>
                        <option value="1953">1953</option>
                        <option value="1952">1952</option>
                        <option value="1951">1951</option>
                        <option value="1950">1950</option>
                        <option value="1949">1949</option>
                        <option value="1948">1948</option>
                        <option value="1947">1947</option>
                        <option value="1946">1946</option>
                        <option value="1945">1945</option>
                        <option value="1944">1944</option>
                        <option value="1943">1943</option>
                        <option value="1942">1942</option>
                        <option value="1941">1941</option>
                    </select>
                </div>
            </div>
        </div>

    </div>

</div>



<!-- Cell Phone -->
<div class="units-row pause">

    <div class="unit-25 end">

        <label class="end" for="js-cell-phone-number">
            Phone (Cell Phone) <span class="req">*</span>
        </label>

    </div>

    <div class="js-form-phone unit-50 end">

        <div class="units-row end">

        @if ($form->mode_verify_phone)

            @if ($verifyPhone['isVerificated'])

            <div class="unit-100 end">
                <div class="field">
                    <input id="js-cell-phone-number" class="width-100 confirmed" type="text" name="cellPhoneNumber" title="Subscriber number" data-theme="blue" data-tools="tooltip" data-parsley-required data-parsley-required-message="Please enter cell phone number" data-parsley-group="js-block-phone" data-parsley-minlength="6" data-parsley-maxlength="15" maxlength="15" data-parsley-trigger="change" data-parsley-minlength-messaage="This phone number is too short. It should have 6 characters or more." data-parsley-phonenumber value="{{ $verifyPhone['cellPhoneNumber'] }}" readonly>
                </div>
            </div>

            @elseif($verifyPhone['isFailed'])

            <div class="unit-100 end">
                <div class="field">
                    <input id="js-cell-phone-number" class="width-100 not-confirmed" type="text" name="cellPhoneNumber" title="Subscriber number" data-theme="blue" data-tools="tooltip" data-parsley-required data-parsley-required-message="Please enter cell phone number" data-parsley-group="js-block-phone" data-parsley-minlength="6" data-parsley-maxlength="15" maxlength="15" data-parsley-trigger="change" data-parsley-minlength-messaage="This phone number is too short. It should have 6 characters or more." data-parsley-phonenumber value="{{ $verifyPhone['cellPhoneNumber'] }}" readonly>
                </div>
            </div>

            @else

                @if ($verifyPhone['attemptsNumber'] <= 1)

                    <div class="unit-100 end">
                        <div class="field">
                            <input id="js-cell-phone-number" class="width-100" type="text" name="cellPhoneNumber" title="Subscriber number" data-theme="blue" data-tools="tooltip" data-parsley-required data-parsley-required-message="Please enter cell phone number" data-parsley-group="js-block-phone" data-parsley-minlength="6" data-parsley-maxlength="15" maxlength="15" data-parsley-trigger="change" data-parsley-minlength-messaage="This phone number is too short. It should have 6 characters or more." data-parsley-phonenumber value="{{ $verifyPhone['cellPhoneNumber'] }}" readonly>
                        </div>
                    </div>

                @else

                    <div class="unit-100 end">
                        <div class="field">
                            <input id="js-cell-phone-number" class="width-100" type="text" name="cellPhoneNumber" title="Subscriber number" data-theme="blue" data-tools="tooltip" data-parsley-required data-parsley-required-message="Please enter cell phone number" data-parsley-group="js-block-phone" data-parsley-minlength="6" data-parsley-maxlength="15" maxlength="15" data-parsley-trigger="change" data-parsley-minlength-messaage="This phone number is too short. It should have 6 characters or more." data-parsley-phonenumber>
                        </div>
                    </div>

                @endif

            @endif

        @else

            <div class="unit-100 end">
                <div class="field">
                    <input id="js-cell-phone-number" class="width-100" type="text" name="cellPhoneNumber" title="Subscriber number" data-theme="blue" data-tools="tooltip" data-parsley-required data-parsley-required-message="Please enter cell phone number" data-parsley-group="js-block-phone" data-parsley-minlength="6" data-parsley-maxlength="15" maxlength="15" data-parsley-trigger="change" data-parsley-minlength-messaage="This phone number is too short. It should have 6 characters or more." data-parsley-phonenumber>
                </div>
            </div>

        @endif

        </div>

        <div class="units-row end">

            <div class="unit-100">

            @if ($form->mode_verify_phone)

                @if ($verifyPhone['isVerificated'])

                    <div class="field">
                        <input id="js-is-phone-confirmed" name="isPhoneConfirmed" type="hidden" value="true" data-parsley-required data-parsley-required-message="Please confirm cell phone number below" data-parsley-trigger="change">
                    </div>

                @elseif ($verifyPhone['isFailed'])

                    <div class="field">
                        <input id="js-is-phone-confirmed" name="isPhoneConfirmed" type="hidden" value="false" data-parsley-required data-parsley-required-message="Please confirm cell phone number below" data-parsley-trigger="change">
                    </div>

                @else

                    @if ($verifyPhone['attemptsNumber'] == 0)

                    <div class="field">
                        <input id="js-is-phone-confirmed" name="isPhoneConfirmed" type="hidden" data-parsley-required data-parsley-required-message="Please confirm cell phone number below" data-parsley-trigger="change" value="false">
                    </div>

                    @else

                    <div class="field">
                        <input id="js-is-phone-confirmed" name="isPhoneConfirmed" type="hidden" data-parsley-required data-parsley-required-message="Please confirm cell phone number below" data-parsley-trigger="change">
                    </div>

                    @endif

                @endif

            @endif

                <div class="forms-desc">Please use international phone number format (example: 18002003040)</div>

            </div>

        </div>

    </div>

</div>


@if ($form->mode_verify_phone)

    @include('applications.blocks.phone-confirmation')

@endif

<!-- E-mail -->
<div class="units-row">

    <div class="unit-25 end">

        <label class="end" for="js-email">
            E-mail <span class="req">*</span>
        </label>

    </div>
    <div class="unit-50 end">

        <div class="field">
            <input id="js-email" type="text" name="email" class="width-100" data-parsley-required data-parsley-required-message="Please enter e-mail" data-parsley-type="email" data-parsley-email-message="Please enter correct e-mail">
        </div>

    </div>

</div>

<br>
