@extends('app')

@section('header')

	<!-- # HEADER -->
	<div class="header units-row">

		<div class="unit-100 unit-max unit-centered">

			<p class="color-white logo end">{{ $form->company->name }}</p>

		</div>

	</div>
	<!-- # END HEADER -->

@stop

@section('wrapper')

	<!-- # MAIN -->
	<div class="wrapper">

		<div class="units-row">

			<div class="form-wrapper unit-100 unit-max unit-centered">

				<p class="color-green"><i class="feather-icon-lock"></i> Encrypted сonnection</p>

				<h1 class="form-header">{{ $form->header }}</h1>

			@if ($form->block_info)

				<nav class="nav-tabs" data-tools="tabs">
					<ul>
						<li class="active"><a href="#js-tab-form">{{ $form->tab_form }}</a></li>
						<li><a href="#js-tab-info">{{ $form->tab_info }}</a></li>
					</ul>
				</nav>

			@endif

				<div id="js-tab-form">

					<div class="js-application-form">

						<div class="markdown-body pause">
							{!! $form->tab_form_content !!}
						</div>

						{!! Form::open(['class' => 'js-form-submit forms', 'method' => 'POST', 'action' => ['ApplicantsController@store', $id], 'files' => true, 'data-prefix' => '', 'data-parsley-validate', 'data-parsley-excluded' => 'input[type=button], input[type=submit],  input[type=reset], [disabled]']); !!}

						{{-- {!! Form::open(['class' => 'forms', 'method' => 'POST', 'action' => ['ApplicantsController@store', $id], 'files' => true, 'data-prefix' => '']); !!} --}}

							@if ($form->block_personal)

								@include('applications.blocks.personal')

							@endif

							@if ($form->block_on_the_web)

								@include('applications.blocks.on-the-web')

							@endif

							@if ($form->block_address)

								@include('applications.blocks.address')

							@endif

							@if ($form->block_address_short)

								@include('applications.blocks.address-short')

							@endif

							@if ($form->block_education)

								@include('applications.blocks.education')

							@endif

							@if ($form->block_experience)

								@include('applications.blocks.experience')

							@endif

							@if ($form->block_hr_info)

								@include('applications.blocks.hr-information')

							@endif

							@if ($form->block_resume)

								@include('applications.blocks.resume')

							@endif

							@include('applications.blocks.signature')

							<!-- Submit form -->
							<div class="units-row">

								<div class="unit-65 unit-push-25">

									<p>
										<button class="js-btn-submit btn btn-blue btn-big width-100">
											<div class="js-btn-submit-spinner ui active mini inline loader inverted js-hide"></div>
											<span id="btn-text">Submit form</span>
										</button>
									</p>

								</div>

							</div>

						{!! Form::close(); !!}

					</div>

					<div class="js-form-submit-success units-row hide">

						<div class="unit-100 unit-max unit-centered">

							<div class="markdown-body pause">{!! $form->thank_you_text !!}</div>

							@if ($form->mode_form_output == 'pdf')

								<div class="units-row">

									<div class="unit-50">

										<p class="pause">1. Download your form PDF print it and sign</p>

										<p>
											<a class="js-pdf-link pdf-link btn btn-blue big" href="#" target="_blank">Download PDF</a>
										</p>

									</div>

									<div class="unit-50">

										<p class="pause">2. Send the signed form by fax number:</p>

										<div class="block-small block-bordered-gray">
											<p class="end">{{ $form->company->fax_number }}</p>
										</div>

									</div>

								</div>

							@endif

							<br>
							<p class="pause">
								<a class="btn btn-blue btn-outline" href="{{ action('ApplicantsController@create', ['id' => $form->hash]) }}">Go back</a>
							</p>

						</div>

					</div>

				</div>

			@if ($form->block_info)

				<div id="js-tab-info" style="display: none;">

					@include('applications.info')

				</div>

			@endif

			</div>

		</div>

	</div>
	<!-- # END MAIN -->

@stop
