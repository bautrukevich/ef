
	<!-- # INFO TAB -->
	<div class="units-row">

	@if (!empty($filesList))

		<div class="unit-75">

	@else

		<div class="unit-100">

	@endif

			{{-- Position Information Here --}}
			<div class="markdown-body">
				{!! $form->tab_info_content !!}
			</div>

		</div>

	@if (!empty($filesList))

		<div class="file-block unit-25">

			<nav class="nav">
				<ul>
				@foreach ($filesList as $file)
					<li class="file text-centered">
						<a class="file-anchor" href="{{ $file['fileLink'] }}"><i class="feather-icon-paper icon"></i><br>{{ $file['fileName'] }}</a>
						<span class="file-size color-gray">{{ $file['fileSize'] }}</span>
					</li>
				@endforeach
				</ul>
			</nav>

		</div>

	@endif

	</div>
	<!-- # END INFO TAB -->
