@extends('pdf')

@section('wrapper')

	<!-- # MAIN -->
	<div class="wrapper">

		<div class="units-row end">

			<div class="unit-100">

				<table class="table-flat">
					<tr>
						<td class="text-left" width="50" valign="top">
							<h2 class="up end">{{ $form->company->name }}</h2>
						</td>
						<td class="text-right" width="50" valign="top">
							<p class="up end">
								Address: {{ $form->company->address }}<br>
								Tel: {{ $form->company->phone_number }}; Fax: {{ $form->company->fax_number }}
							</p>
						</td>
					</tr>
				</table>

			</div>

		</div>

		<div class="units-row end">

			<div class="unit-100 unit-centered end">

				<h4>{{ $form->header }}</h4>

				<table class="table-flat table-stripped end">

				@if ($form->block_personal)

					{{-- <tr>
						<td colspan="2">
							<h4 class="end">Personal Information</h4>
						</td>
					</tr> --}}

					<tr width="40">
						<td>Full name</td>
						<td>{{ isset($firstName) ? $firstName : '———' }} {{ isset($middleName) ? $middleName : '———' }} {{ isset($lastName) ? $lastName : '———' }}</td>
					</tr>
					<tr>
						<td>Date of birth</td>
						<td>{{ isset($bDate) ? $bDate : '———' }}</td>
					</tr>
					<tr>
						<td>Cell phone</td>
						<td>{{ isset($cellPhone) ? $cellPhone : '———' }} {{ isset($isPhoneConfirmed) ? "(".$isPhoneConfirmed.")" : '' }}</td>
					</tr>
					<tr>
						<td>Email</td>
						<td>{{ isset($email) ? $email : '———' }}</td>
					</tr>

				@endif

				@if ($form->block_on_the_web)

					<tr width="40">
						<td>LinkedIn</td>
						<td>{{ isset($linkedin) ? $linkedin : '———' }}</td>
					</tr>
					<tr>
						<td>Facebook</td>
						<td>{{ isset($facebook) ? $facebook : '———' }}</td>
					</tr>

				@endif

				@if ($form->block_education)

					<tr width="40">
						<td>Institution</td>
						<td>{{ isset($institution) ? $institution : '———' }}</td>
					</tr>
					<tr>
						<td>Degree</td>
						<td>{{ isset($degree) ? $degree : '———' }}</td>
					</tr>
					<tr>
						<td>Major</td>
						<td>{{ isset($major) ? $major : '———' }}</td>
					</tr>
					<tr>
						<td>Country</td>
						<td>{{ isset($educationCountry) ? $educationCountry : '———' }}</td>
					</tr>
					<tr>
						<td>Start of studying</td>
						<td>{{ isset($educationStart) ? $educationStart : '———' }}</td>
					</tr>
					<tr>
						<td>End of studying</td>
						<td>{{ isset($educationEnd) ? $educationEnd : '———' }}</td>
					</tr>
					<tr>
						<td>Description</td>
						<td>{{ isset($educationDescription) ? $educationDescription : '———' }}</td>
					</tr>

				@endif

				@if ($form->block_address)

					{{-- <tr>
						<td colspan="2">
							<h4 class="end">Contact Information</h4>
						</td>
					</tr> --}}

					<tr>
						<td>Address line 1</td>
						<td>{{ isset($addressLine1) ? $addressLine1 : '———' }}</td>
					</tr>
					<tr>
						<td>Address line 2</td>
						<td>{{ isset($addressLine2) ? $addressLine2 : '———' }}</td>
					</tr>
					<tr>
						<td>City</td>
						<td>{{ isset($city) ? $city : '———' }}</td>
					</tr>
					<tr>
						<td>State / Province / Region</td>
						<td>{{ isset($region) ? $region : '———' }}</td>
					</tr>
					<tr>
						<td>ZIP / Postal code</td>
						<td>{{ isset($zip) ? $zip : '———' }}</td>
					</tr>
					<tr>
						<td>Country</td>
						<td>{{ isset($addressCountry) ? $addressCountry : '———' }}</td>
					</tr>

				@endif

				@if ($form->block_hr_info)

					{{-- <tr>
						<td colspan="2">
							<h4 class="end">HR Information</h4>
						</td>
					</tr> --}}

					<tr>
						<td>Employment status</td>
						<td>{{ isset($employmentStatus) ? $employmentStatus : '———' }}</td>
					</tr>
					<tr>
						<td>When able to start</td>
						<td>{{ isset($employmentStart) ? $employmentStart : '———' }}</td>
					</tr>
					<tr>
						<td>Preferred job type</td>
						<td>{{ isset($preferredJobType) ? $preferredJobType : '———' }}</td>
					</tr>
					<tr>
						<td>Best time to call</td>
						<td>{{ isset($bestTimeToCall) ? $bestTimeToCall : '———' }}</td>
					</tr>

				@endif

				@if ($form->block_experience)

					{{-- <tr>
						<td colspan="2">
							<h4 class="end">Employment History</h4>
						</td>
					</tr> --}}

					<tr>
						<td>Occupation / Title</td>
						<td>{{ isset($occupation) ? $occupation : '———' }}</td>
					</tr>
					<tr>
						<td>Company / Business name</td>
						<td>{{ isset($company) ? $company : '———' }}</td>
					</tr>
					<tr>
						<td>Description</td>
						<td>{{ isset($experienceDescription) ? $experienceDescription : '———' }}</td>
					</tr>
					<tr>
						<td>Salary</td>
						<td>{{ isset($salary) ? $salary : '———' }}</td>
					</tr>
					<tr>
						<td>Start of employment</td>
						<td>{{ isset($employmentHistoryStart) ? $employmentHistoryStart : '———' }}</td>
					</tr>
					<tr>
						<td>End of employment</td>
						<td>{{ isset($employmentHistoryEnd) ? $employmentHistoryEnd : '———' }}</td>
					</tr>

				@endif

					<tr>
						<td>Date from form</td>
						<td>{{ isset($date) ? $date : '———' }}</td>
					</tr>
					<tr>
						<td>Detected IP</td>
						<td>{{ isset($ip) ? $ip : '———' }}</td>
					</tr>
					<tr>

						<td valign="middle">Your signature</td>
						<td valign="middle">

						@if (isset($signatureFile))

							<img src="{{ $signatureFile }}" alt="Signature" height="40"> / {{ isset($firstName) ? $firstName : '———' }} {{ isset($lastName) ? $lastName : '———' }} /

						@else

							{{ '_______________' }} / {{ isset($firstName) ? $firstName : '———' }} {{ isset($lastName) ? $lastName : '———' }} /

						@endif

						</td>

					</tr>
				</table>

			</div>

		</div>

	</div>
	<!-- # END MAIN -->

@stop
