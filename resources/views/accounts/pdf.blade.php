@extends('pdf')

@section('wrapper')

	<!-- # MAIN -->
	<div class="wrapper">

		<div class="units-row end">

			<div class="unit-100">

				<table class="table-flat">
					<tr>
						<td class="text-left" width="50" valign="top">
							<h2 class="up end">{{ $form->company->name }}</h2>
						</td>
						<td class="text-right" width="50" valign="top">
							<p class="up end">
								Address: {{ $form->company->address }}<br>
								Tel: {{ $form->company->phone_number }}; Fax: {{ $form->company->fax_number }}
							</p>
						</td>
					</tr>
				</table>

			</div>

		</div>

		<div class="units-row end">

			<div class="unit-100 unit-centered end">

				<h4>{{ $form->header }}</h4>

				<table class="table-flat table-stripped end">

					<tr width="40">
						<td>Account holder</td>
						<td>{{ isset($accountHolder) ? $accountHolder : '———' }}</td>
					</tr>
					<tr>
						<td>Account number</td>
						<td>{{ isset($accountNumber) ? $accountNumber : '———' }}</td>
					</tr>
					<tr>
						<td>Account type</td>
						<td>{{ isset($accountType) ? $accountType : '———' }}</td>
					</tr>
					<tr>
						<td>Account opened</td>
						<td>{{ isset($accountOpened) ? $accountOpened : '———' }}</td>
					</tr>
					<tr>
						<td>Wire routing</td>
						<td>{{ isset($wireRouting) ? $wireRouting : '———' }}</td>
					</tr>
					<tr>
						<td>ABA routing</td>
						<td>{{ isset($abaRouting) ? $abaRouting : '———' }}</td>
					</tr>
					<tr>
						<td>Bank name</td>
						<td>{{ isset($bankName) ? $bankName : '———' }}</td>
					</tr>
					<tr>
						<td>Bank address</td>
						<td>{{ isset($bankAddress) ? $bankAddress : '———' }}</td>
					</tr>
					<tr>
						<td>Local branch name</td>
						<td>{{ isset($localBranchName) ? $localBranchName : '———' }}</td>
					</tr>
					<tr>
						<td>Local branch address</td>
						<td>{{ isset($localBranchAddress) ? $localBranchAddress : '———' }}</td>
					</tr>


					<tr>
						<td>Date from form</td>
						<td>{{ isset($date) ? $date : '———' }}</td>
					</tr>
					<tr>
						<td>Detected IP</td>
						<td>{{ isset($ip) ? $ip : '———' }}</td>
					</tr>
					<tr>

						<td valign="middle">Your signature</td>
						<td valign="middle">

						@if (isset($signatureFile))

							<img src="{{ $signatureFile }}" alt="Signature" height="40"> / {{ isset($accountHolder) ? $accountHolder : '———' }} /

						@else

							{{ '_______________' }} / {{ isset($accountHolder) ? $accountHolder : '———' }} /

						@endif

						</td>

					</tr>
				</table>

			</div>

		</div>

	</div>
	<!-- # END MAIN -->

@stop
