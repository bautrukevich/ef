<h2 class="background-gray">Void check</h2>

<!-- File upload -->
<div class="units-row">

    <div class="unit-25 end">

        <label class="end" for="js-file-upload">
            Void check
        </label>

    </div>
    <div class="unit-50 end">

        <div class="js-fileinput fileinput-new" data-provides="fileinput">
            <span class="btn btn-gray btn-file btn-outline">
                <span class="fileinput-new">Select file</span>
                <span class="fileinput-exists">Change</span>
                <input id="js-file-type-void-check" type="file" name="voidCheck" data-parsley-pattern="/([а-яА-Яa-zA-Z0-9\s_\\.\-:])+(.pdf|.jpg|.jpeg|.bmp|.png)$/" data-parsley-pattern-message="The file must be a file of type: jpg, jpeg, bmp, png, pdf" data-parsley-errors-container="#js-void-check-errors" data-parsley-trigger="change">
            </span>
            <span class="fileinput-filename"></span>
            <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
        </div>
        <div id="js-void-check-errors"></div>
        <div class="field">
            <div class="js-void-check-file-types forms-desc">The file must be a file of type: jpg, jpeg, bmp, png, pdf</div>
        </div>

    </div>

</div>

<br>
