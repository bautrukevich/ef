<h2 class="background-gray">Account info</h2>

<!-- Account holder -->
<div class="units-row">

    <div class="unit-25 end">

        <label class="end" for="js-account-holder">
            Account holder <span class="req">*</span>
        </label>

    </div>
    <div class="unit-50 end">

        <div class="field">
            <input id="js-account-holder" type="text" name="accountHolder" class="js-capitalize caps width-100" data-parsley-required data-parsley-required-message="Please enter account holder">
        </div>

    </div>

</div>

<!-- Account number -->
<div class="units-row">

    <div class="unit-25 end">

        <label class="end" for="js-account-number">
            Account number <span class="req">*</span>
        </label>

    </div>
    <div class="unit-50 end">

        <div class="field">
            <input id="js-account-number" type="text" name="accountNumber" class="js-capitalize caps width-100" data-parsley-required data-parsley-required-message="Please enter account number" data-parsley-maxlength="20" maxlength="20">
        </div>

    </div>

</div>

<!-- Account type -->
<div class="units-row">

    <div class="unit-25 end">

        <label class="end" for="js-account-type">
            Account type <span class="req">*</span>
        </label>

    </div>
    <div class="unit-50 end">

        <div class="field">
            <select id="js-account-type" name="accountType" data-parsley-required data-parsley-required-message="Please choose account type">
                <option value="">Choose account type</option>
                <option value="Checking">Checking</option>
                <option value="Savings">Savings</option>
                <option value="Business Checking">Business Checking</option>
            </select>
        </div>

    </div>

</div>

<!-- Account opened -->
<div class="units-row">

    <div class="unit-25 end">

        <label class="end" for="js-account-opened">
            Account opened <span class="req">*</span>
        </label>

    </div>
    <div class="unit-50 end">

        <div class="field">

            <select id="js-account-opened" name="accountOpened" data-parsley-required data-parsley-required-message="Please enter account opened year">
                <option value="">Choose year</option>
                <option value="2016">2016</option>
                <option value="2015">2015</option>
                <option value="2014">2014</option>
                <option value="2013">2013</option>
                <option value="2012">2012</option>
                <option value="2011">2011</option>
                <option value="2010">2010</option>
                <option value="2009">2009</option>
                <option value="2008">2008</option>
                <option value="2007">2007</option>
                <option value="2006">2006</option>
                <option value="2005">2005</option>
                <option value="2004">2004</option>
                <option value="2003">2003</option>
                <option value="2002">2002</option>
                <option value="2001">2001</option>
                <option value="2000">2000</option>
                <option value="1999">1999</option>
                <option value="1998">1998</option>
                <option value="1997">1997</option>
                <option value="1996">1996</option>
                <option value="1995">1995</option>
                <option value="1994">1994</option>
                <option value="1993">1993</option>
                <option value="1992">1992</option>
                <option value="1991">1991</option>
                <option value="1990">1990</option>
                <option value="1989">1989</option>
                <option value="1988">1988</option>
                <option value="1987">1987</option>
                <option value="1986">1986</option>
                <option value="1985">1985</option>
                <option value="1984">1984</option>
                <option value="1983">1983</option>
                <option value="1982">1982</option>
                <option value="1981">1981</option>
            </select>
        </div>

    </div>

</div>

<!-- Wire routing -->
<div class="units-row">

    <div class="unit-25 end">

        <label class="end" for="js-wire-routing">
            Wire routing <span class="req">*</span>
        </label>

    </div>
    <div class="unit-50 end">

        <div class="field">
            <input id="js-wire-routing" type="text" name="wireRouting" class="js-capitalize caps width-100" data-parsley-required data-parsley-required-message="Please enter wire routing number"  data-parsley-maxlenght="20">
        </div>
        <div class="js-wire-routing forms-desc">Wire routing number is a number used to receive incoming domestic or international wire transfers.</div>

    </div>

</div>

<!-- Aba routing -->
<div class="units-row">

    <div class="unit-25 end">

        <label class="end" for="js-aba-routing">
            ABA routing <span class="req">*</span>
        </label>

    </div>
    <div class="unit-50 end">

        <div class="field">
            <input id="js-aba-routing" type="text" name="abaRouting" class="js-capitalize caps width-100" data-parsley-required data-parsley-required-message="Please enter ABA routing number">
        </div>
        <div class="js-aba-routing forms-desc">ABA routing number is a number for ACH transactions, to set up direct deposits or outgoing payments to other financial institutions.</div>

    </div>

</div>

<!-- Bank name -->
<div class="units-row">

    <div class="unit-25 end">

        <label class="end" for="js-bank-name">
            Bank name <span class="req">*</span>
        </label>

    </div>
    <div class="unit-65 end">

        <div class="field">
            <input id="js-bank-name" type="text" name="bankName" class="js-capitalize caps width-100" data-parsley-required data-parsley-required-message="Please enter bank name">
        </div>

    </div>

</div>

<!-- Bank address -->
<div class="units-row">

    <div class="unit-25 end">

        <label class="end" for="js-bank-address">
            Bank address <span class="req">*</span>
        </label>

    </div>
    <div class="unit-65 end">

        <div class="field">
            <input id="js-bank-address" type="text" name="bankAddress" class="js-capitalize caps width-100" data-parsley-required data-parsley-required-message="Please enter bank address">
        </div>

    </div>

</div>

<!-- Local branch name -->
<div class="units-row">

    <div class="unit-25 end">

        <label class="end" for="js-local-branch-name">
            Local branch name <span class="req">*</span>
        </label>

    </div>
    <div class="unit-65 end">

        <div class="field">
            <input id="js-local-branch-name" type="text" name="localBranchName" class="js-capitalize caps width-100" data-parsley-required data-parsley-required-message="Please enter local branch name">
        </div>

    </div>

</div>

<!-- Local branch address -->
<div class="units-row">

    <div class="unit-25 end">

        <label class="end" for="js-local-branch-address">
            Local branch address <span class="req">*</span>
        </label>

    </div>
    <div class="unit-65 end">

        <div class="field">
            <input id="js-local-branch-address" type="text" name="localBranchAddress" class="js-capitalize caps width-100" data-parsley-required data-parsley-required-message="Please enter local branch address">
        </div>

    </div>

</div>

<br>
