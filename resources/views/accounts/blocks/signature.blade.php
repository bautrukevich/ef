<h2 class="background-gray">Signature</h2>

<!-- Agreement -->
<div class="units-row">

    <div class="unit-25 end">

        <label class="end">
            Agreement
        </label>

    </div>
    <div class="unit-65 end">

        <div class="block-blue agreement markdown-body">
            {!! $form->tab_form_agreement !!}
        </div>

    </div>

</div>

<!-- Electronic signature -->
<div class="units-row">

    <div class="unit-25 end">

        <label class="end">
            Electronic signature
            @if ($form->block_signature)
            <br><span class="color-gray-dark">(draw with mouse or finger)</span>
            @endif
        </label>

    </div>
    <div class="unit-65 end">

        <div class="block block-bordered-gray">

            @if ($form->block_signature)

            <div class="units-row pause">

                <!-- SIGNATURE -->
                <div class="unit-100 relative">

                    <div id="js-signature-pad" class="m-signature-pad">
                        <div class="m-signature-pad--body">
                            <canvas></canvas>
                        </div>
                    </div>

                    <div class="field">
                        <input class="js-signature-output-input" id="js-signature-output-id" type="hidden" name="signatureOutput">
                        <div id="js-signature-errors"></div>
                    </div>

                </div>
                <!-- END SIGNATURE -->

            </div>

            <input class="js-signature-output-input" id="js-signature-output-id" type="hidden" name="signatureOutput" data-parsley-required data-parsley-required-message="Please sign document">

            @endif

            <div class="units-row end">
                <div class="unit-75 text-left">
                    <p class="js-full-name end"></p>
                    <p class="end">Date & Time: {{ $now }}<br>IP Address: {{ $ip }}</p>
                    <input type="hidden" name="date" value="{{ $now }}">
                    <input type="hidden" name="ip" value="{{ $ip }}">
                </div>
                @if ($form->block_signature)
                <div class="unit-25 end text-right">
                    <a class="js-clear btn btn-gray btn-outline">Clear</a>
                </div>
                @endif
            </div>

        </div>

    </div>

</div>
