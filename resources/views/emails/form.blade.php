@if(env('PERSONAL'))
<b>PERSONAL</b><br>
First name: {{ isset($firstName) ? $firstName : '———' }}<br>
Middle name: {{ isset($middleName) ? $middleName : '———' }}<br>
Last name: {{ isset($lastName) ? $lastName : '———' }}<br>
Date of birth: {{ isset($bDate) ? $bDate : '———' }}<br>
Cell phone: {{ isset($cellPhone) ? $cellPhone : '———' }} {{ isset($phoneIsConfirmed) ? "(".$phoneIsConfirmed.")" : '' }}<br>
E-mail: {{ isset($email) ? $email : '———' }}<br>
<br>
@endif

@if(env('ON_THE_WEB'))
<b>ON THE WEB</b><br>
LinkedIn: {{ isset($linkedin) ? $linkedin : '———' }}<br>
Facebook: {{ isset($facebook) ? $facebook : '———' }}<br>
Twitter: {{ isset($twitter) ? $twitter : '———' }}<br>
<br>
@endif

@if(env('EDUCATION'))
<b>EDUCATION</b><br>
Institution: {{ isset($institution) ? $institution : '———' }}<br>
Degree: {{ isset($degree) ? $degree : '———' }}<br>
Major: {{ isset($major) ? $major : '———' }}<br>
Country: {{ isset($educationCountry) ? $educationCountry : '———' }}<br>
Start of studying: {{ isset($educationStart) ? $educationStart : '———' }}<br>
End of studying: {{ isset($educationEnd) ? $educationEnd : '———' }}<br>
Description: {{ isset($educationDescription) ? $educationDescription : '———' }}<br>
<br>
@endif

@if(env('ADDRESS'))
<b>ADDRESS</b><br>
Address line 1: {{ isset($addressLine1) ? $addressLine1 : '———' }}<br>
Address line 2: {{ isset($addressLine2) ? $addressLine2 : '———' }}<br>
City: {{ isset($city) ? $city : '———' }}<br>
State / Province / Region: {{ isset($region) ? $region : '———' }}<br>
ZIP / Postal code: {{ isset($zip) ? $zip : '———' }}<br>
Country: {{ isset($country) ? $country : '———' }}<br>
<br>
@endif

@if(env('HR_INFORMATION'))
<b>HR INFORMATION</b><br>
Employment status: {{ isset($employmentStatus) ? $employmentStatus : '———' }}<br>
When able to start: {{ isset($employmentStart) ? $employmentStart : '———' }}<br>
Preferred job type: {{ isset($preferredJobType) ? $preferredJobType : '———' }}<br>
Best time to call: {{ isset($bestTimeToCall) ? $bestTimeToCall : '———' }}<br>
<br>
@endif

@if(env('EXPERIENCE'))
<b>EXPERIENCE</b><br>
Occupation / Title: {{ isset($occupation) ? $occupation : '———' }}<br>
Company / Business name: {{ isset($company) ? $company : '———' }}<br>
Description: {{ isset($company) ? $company : '———' }}<br>
Salary: {{ isset($company) ? $company : '———' }}<br>
Start of employment: {{ isset($employmentHistoryStart) ? $employmentHistoryStart : '———' }}<br>
End of employment: {{ isset($employmentHistoryEnd) ? $employmentHistoryEnd : '———' }}<br>
<br>
@endif

<b>SIGNATURE</b><br>
@if(env('SIGNATURE'))

@if (isset($signatureFile))
	<br>
	<img src="{{ $message->embed($signatureFileName) }}" style="height: 30px;"><br><br>
	/ {{ isset($firstName) ? $firstName : '———' }} {{ isset($lastName) ? $lastName : '———' }} /<br>

@else

	Electronic signature: {{ isset($firstName) ? $firstName : '———' }} {{ isset($lastName) ? $lastName : '———' }}<br>

@endif
Date from form: {{ isset($date) ? $date : '———' }}<br>
Detected IP: {{ isset($ip) ? $ip : '———' }}<br>
<br>
@else
Electronic signature: {{ isset($firstName) ? $firstName : '———' }} {{ isset($lastName) ? $lastName : '———' }}<br>
Date from form: {{ isset($date) ? $date : '———' }}<br>
Detected IP: {{ isset($ip) ? $ip : '———' }}<br>
@endif
<br>
